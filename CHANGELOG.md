CHANGELOG
=========

2023-05-02 (1.0.1)
------------------
- :func:`collect_choi_states_from_csv` now tries to cast the `number_of_states` parameter to `int` such that a larger range of inputs is accepted (e.g., `1E6`).

2023-04-25 (1.0.0)
------------------
- Created this snippet based on code in private repository.

