NetSquid-TreeCode (1.0.1)
================================

Description
-----------

This is a user contributed _snippet_ for the [NetSquid quantum network simulator](https://netsquid.org).

The purpose of this snippet is the simulation of so-called tree codes.
Tree codes are quantum error-correcting codes based on tree states (i.e., graph states corresponding to tree graphs).
Tree codes are useful, for example, to build one-way quantum repeaters as they offer good protection against loss channels (see, e.g., the paper "One-Way Quantum Repeater Based on Near-Deterministic Photon-Emitter Interfaces" by Borregaard et al., https://arxiv.org/abs/1907.05101).

The core of this snippet is contained in `tree_code.py`, which contains the class `Tree` for representing qubits encoded using a tree code.
For the other files:
- `repeater_chain.py` contains code for simulating a one-way repeater chain based on tree codes.
- `tree_channel_error_probability.py` contains code for characterizing the effective logical-level quantum channel induced by subjecting every lower-level qubit in the code to loss and errors.
- `stabilizer_measure_function.py` contains code for performing stabilizer measurements on a number of logical tree-encoded qubit (this can be useful when, e.g., concatenating the tree code with a stabilizer code).
- `models.py` contains a pure loss model that can be used to model losses of the type that tree codes protect against.

The code in this snippet was largely developed as part of the master's graduation project of Maria Flors Mor-Ruiz at TU Delft, which focussed on concatenating the tree code with a stabilizer code to build a quantum repeater that combines loss tolerance with error tolerance.
The corresponding thesis can be found [here](https://repository.tudelft.nl/islandora/object/uuid:965b09c7-c652-40d5-8f14-2eea99f99983).


Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.


Documentation
-------------

How to build the docs:

First build the docs by:

```bash
make build-docs
```

This will first install any required dependencies and build the html files.

To open the built docs (using 'open' or 'xdg-open'), do:

```bash
make open-docs
```

To both build the html files and open them, do:
```bash
make docs
```


Contributors
------------

- Maria Flors Mor-Ruiz
- Guus Avis (maintainer, g.avis@tudelft.nl)


License
-------

The snippet NetSquid-TreeCode has the following license:

> Copyright 2020 QuTech (TUDelft and TNO)
>
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
>
>     http://www.apache.org/licenses/LICENSE-2.0
>
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
