API Documentation
-----------------

Below are the modules of this package.

See the README for a high-level overview.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/tree_code
   modules/repeater_chain
   modules/tree_channel_error_probability
   modules/stabilizer_measure_function
   modules/models
