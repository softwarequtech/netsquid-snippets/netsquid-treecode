models
------

Noise and loss models that can be applied to qubits.


.. automodule:: netsquid_treecode.models
    :members:
