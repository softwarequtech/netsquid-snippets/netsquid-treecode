repeater_chain
--------------

Allows for the simulation of quantum repeaters based on tree codes (see https://arxiv.org/abs/1907.05101 for reference).


.. automodule:: netsquid_treecode.repeater_chain
    :members:
