tree_channel_error_probability
------------------------------

Tools for characterizing the effective logical-level channel induced by applying loss and errors on the lower-level qubits of a tree-encoded qubit.


.. automodule:: netsquid_treecode.tree_channel_error_probability
    :members:
