tree_code
---------

Main module of this snippet.
Allows for the simulation of tree codes.

Note: tree codes can contain up to many trees, therefore it is recommended that netsquid is set to a formalism that can handle that.
As tree-encoded qubits are stabilizer states, e.g., the stabilizer formalism can be used.

.. automodule:: netsquid_treecode.tree_code
    :members:
