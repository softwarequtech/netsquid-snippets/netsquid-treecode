import netsquid as ns
from netsquid_treecode.tree_channel_error_probability import collect_choi_states_from_csv
import os


def main(no_output=False, save_data=False):
    ns.set_qstate_formalism(ns.QFormalism.STAB)
    path = "examples/example_csv_generated_data"
    if not no_output:
        print("Start collecting Choi states corresponding to the quantum channel defined by transmitting"
              " a tree-encoded qubit through a noisy and lossy channel, as specified in the example CSV file.")
    collect_choi_states_from_csv(csv_specification_filename="examples/"
                                                            "example_tree_channel_error_probability_parameters.csv",
                                 number_of_states=10,
                                 path=path)
    if not no_output:
        print("Succeeded at collecting Choi-state data!")
        if save_data:
            print(f"Saved the data in the directory {path}.")
    if not save_data:
        for file in os.listdir(path):
            if file[-4:] == ".csv":
                os.remove(f"{path}/{file}")
        os.rmdir(path)


if __name__ == "__main__":
    main(no_output=False, save_data=False)
