from netsquid_treecode.repeater_chain import analytical_success_probability, tree_code_repeater_chain_qkd
from netsquid_simulationtools.repchain_data_process import process_qkd_data
import csv
import numpy as np
from netsquid.qubits.qformalism import QFormalism, set_qstate_formalism
import matplotlib.pyplot as plt
import pandas
import timeit
import os


def generate_analytical_data(branching_vector, path, loss_prob_min=0., loss_prob_max=0.5):
    """This function generates many points of data of the analytical expression for the total number of attempts needed
    for success in terms of loss of a tree-code-based one-way quantum repeater, for a certain branching
    vector. The analytical expression can be found in the following paper:
        https://arxiv.org/abs/1907.05101 (equation 5).

    Parameters
    ---------
    branching_vector : list of int
    path : str
        Path were the pickle files with the raw data will be stored.
    loss_prob_min : float
        Smallest loss probability to consider.
    loss_prob_max : float
        Largest loss probability to consider.

    """
    loss_probabilities = []
    attempts_success = []
    for loss_probability in np.linspace(loss_prob_min, loss_prob_max):
        attempt_success = 1/analytical_success_probability(branching_vector, loss_probability)
        loss_probabilities.append(loss_probability)
        attempts_success.append(attempt_success)
    with open(path + '/analytical_data.csv', 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        column_names = ['loss_probability', 'attempts_per_success']
        csv_writer.writerow(column_names)
        for [loss_probability, attempt_success] in zip(loss_probabilities, attempts_success):
            csv_writer.writerow([loss_probability, attempt_success])
    return


def _set_font_sizes(small=17, medium=20, large=24, usetex=False):
    """Set font sizes of the plot.

    Parameters
    ----------
    small : float (optional)
        Size of default font, axes labels, ticks and legend.
    medium : float (optional)
        Fontsize of x and y labels.
    large : float (optional)
        Fontsize of figure title.
    usetex : bool (optional)
        Whether to use latex for all text output. Default is False.
    """

    plt.rc('font', size=small)          # controls default text sizes
    plt.rc('axes', titlesize=medium)     # fontsize of the axes title
    plt.rc('axes', labelsize=large)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=small)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=small)    # fontsize of the tick labels
    plt.rc('legend', fontsize=medium)    # legend fontsize
    plt.rc('figure', titlesize=large)   # fontsize of the figure title
    plt.rc('text', usetex=usetex)         # use LaTeX for text output


def plot_verification(filename_simulation, filename_analytic, path, save_filename=None, shaded=True,
                      save_formats=(".eps", ".svg", ".pdf", ".png")):
    """Read a combined container with processed QKD data and plots number of attempts per success over the loss
    probability and also plots the analytic version from the formula found in the following paper:
        https://arxiv.org/abs/1907.05101
    Specifically, this equation is equivalent to equation number 5 in the paper.

    Parameters
    ----------
    filename_simulation : str
        Filename of a csv file containing the combined processed QKD data and the varied parameter. This .csv file can
        be generated with :meth:`process_qkd_data` in `repchain_data_process.py`. The .csv file has to have the
        following columns: scan_param_name, 'sk_rate', 'sk_rate_lower_bound', 'sk_rate_upper_bound', 'Qber_x', 'Qber_z',
        and 'attempts_per_success'.
    filename_analytic : str
        Filename of a csv file containing the data of the loss probability and
    path : str
        Path where the output plots will be saved
    save_filename : str (optional)
        Name of the file the figure should be saved as.
        Default is None which creates a name for the saved plot using the name of the input file (filename).
    shaded : Boolean (optional)
        Whether the SKR data should have shaded visualisation of the error.
    save_formats : String or tuple of Strings
        Defines the file formats in which the resulting plot should be saved.

    Returns
    -------
    plt : `matplotlib.pyplot`
        Object with resulting plot.
    """
    sim = pandas.read_csv(filename_simulation)
    sim = sim.sort_values(by="loss_probability")
    analytic = pandas.read_csv(filename_analytic)
    analytic = analytic.sort_values(by='loss_probability')

    # Create plot
    _set_font_sizes()
    fig, axis = plt.subplots(1, 1, figsize=(6, 5))

    # Plot attempts per success
    sim.plot(x="loss_probability", y="attempts_per_success", kind="scatter", color='green',
             yerr="attempts_per_success_error", label='Simulation', ax=axis, logy=False, grid=True)
    analytic.plot(x='loss_probability', y='attempts_per_success', ax=axis, logy=False,
                  grid=True, color='blue', label='Analytic')

    axis.set_ylabel("Avg. num. of att./succ.")
    axis.set_xlabel("Loss probability")
    axis.legend()

    # Save figure
    save_filename = save_filename if save_filename is not None \
        else path + "/" + filename_simulation.split("/")[-1][:-4]
    if type(save_formats) is str:
        fig.savefig(save_filename + save_formats, bbox_inches="tight")
    else:
        for fileformat in save_formats:
            fig.savefig(save_filename + fileformat, bbox_inches="tight")

    plt.show()
    return plt


if __name__ == "__main__":
    set_qstate_formalism(QFormalism.GSLC)
    loss_prob_min = 0.01
    loss_prob_max = 0.1
    num_data_points = 10
    error_probability = 0.
    b_v = [2, 2, 2]
    iterations = 100
    directory = "repeater_chain_example_data"
    if not os.path.exists(directory):
        os.makedirs(directory)
    generate_analytical_data(b_v, directory, loss_prob_min=loss_prob_min,
                             loss_prob_max=loss_prob_max)

    # Do simulations
    for loss_probability in np.linspace(loss_prob_min, loss_prob_max, num=num_data_points):
        start = timeit.default_timer()
        tree_code_repeater_chain_qkd(iterations, 4, b_v, loss_probability, error_probability, directory)
        end = timeit.default_timer()
        time = end - start
        print('Computed for loss probability', loss_probability, 'in time:', time)

    process_qkd_data(directory, suffix='.pickle',
                     output=directory + '/combined_data.pickle',
                     plot_processed_data=False,
                     csv_output_files=(directory + '/output.csv', directory + '/csv_output.csv'))

    plot_verification(directory + '/output.csv', directory + '/analytical_data.csv', directory, shaded=False)
