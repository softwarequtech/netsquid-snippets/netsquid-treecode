from netsquid.components import QuantumErrorModel, DepolarNoiseModel
import netsquid.qubits.qubitapi as qapi


class PureLossModel(QuantumErrorModel):
    """Model for qubit loss with fixed loss probability."""
    def __init__(self, loss_probability, **kwargs):
        super().__init__(**kwargs)
        self.loss_probability = loss_probability

    @property
    def loss_probability(self):
        return self.properties["loss_probability"]

    @loss_probability.setter
    def loss_probability(self, loss_probability):
        if not 0. <= loss_probability <= 1.:
            raise ValueError("{} is not a valid probability".format(loss_probability))
        self.properties["loss_probability"] = loss_probability

    def error_operation(self, qubits, delta_time=0, **kwargs):
        """Error operation to apply to qubits.

        Parameters
        ----------
        qubits : tuple of :obj:`~netsquid.qubits.qubit.Qubit`
            Qubits to apply noise to.
        delta_time : float, optional
            Time qubits have spent on a component [ns]. Only used if
            :attr:`~netsquid.components.models.qerrormodels.DepolarNoiseModel.time_independent` is False.

        """
        for qubit_index in range(len(qubits)):
            self.lose_qubit(qubits=qubits, qubit_index=qubit_index, prob_loss=self.loss_probability)


if __name__ == "__main__":
    loss_noise_model = PureLossModel(loss_probability=0.3) + DepolarNoiseModel(depolar_rate=0.1, time_independent=True)
    qubit = qapi.create_qubits(1)
    loss_noise_model(qubit)
    print(qubit[0])
