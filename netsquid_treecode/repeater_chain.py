import copy
import random
import pickle
import netsquid.qubits.qubitapi as qapi
import netsquid.qubits.operators as ops
from netsquid_treecode.tree_code import Tree
from netsquid_treecode.models import PureLossModel
from netsquid_treecode.models import DepolarNoiseModel
from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder


def tree_code_repeater_chain(qubit, num_of_links, branching_vector, loss_probability, depolar_rate):
    """ This function takes a qubit and encodes it in a tree of a certain branching vector

    Parameters
    ----------
    qubit : :class:`netsquid.qubits.qubit.Qubit`
    num_of_links : int
        Number of links in the chain
    branching_vector : list of int
    loss_probability : value from 0 to 1
        Probability to lose a physical qubit as it is transmitted between two repeater stations
    depolar_rate : value from 0 to 1
        Probability of having depolarizing error in a physical qubit as it is transmitted between two repeater stations


    Returns
    -------
    data_qubit: :class:`netsquid.qubits.qubit.Qubit`
        State of the final qubit after having gone through repeater stations and decoded of the tree encoding.
        None, in case that the decoding or heralded storage was not possible due to losses.

    """
    data_qubit = qubit
    for i in range(num_of_links):
        tree = Tree(data_qubit, branching_vector)
        qubits = tree.qubits
        loss_noise_model = PureLossModel(loss_probability) + DepolarNoiseModel(depolar_rate, time_independent=True)
        loss_noise_model(qubits)
        hs = tree.heralded_storage()
        if hs[0] is None:
            return None
        success = tree.decode_tree(hs[0], hs[1])
        if success is False:
            return None
        data_qubit = hs[1]
    return data_qubit


def tree_code_repeater_chain_qkd(success_iterations, num_of_links, branching_vector, loss_probability, depolar_rate,
                                 path):
    """This functions performs an estimation of the secret key rate of the one way quantum repeater using only the tree
    code. In this case, Alice prepares her qubit in the Z or X basis and Bob measures the qubit that he gets in the same
    basis that Alice chose.

    Parameters
    ----------
    success_iterations: int
        number of times that the protocol is done successfully
    num_of_links : int
        number of repeaters in the chain
    branching_vector : list of int
    loss_probability : number from 0 to 1
        Probability to lose a qubit
    depolar_rate : number from 0 to 1
        Probability of having a depolarizing error in a qubit
    path : str
        Path were the pickle files with the raw data will be stored

    """
    # Create a dictionary to store the data
    data = {"basis_A": [], "basis_B": [], "outcome_A": [], "outcome_B": [], "number_of_rounds": [],
            "midpoint_outcome_0": []}
    # Create the baseline_parameters that characterize the simulation
    baseline_parameters = {"num_of_links": num_of_links, "branching_vector": tuple(branching_vector),
                           "loss_probability": loss_probability, "depolar_rate": depolar_rate, "length": 10}
    for basis_choice, measurement_operator in zip(["X", "Z"], [ops.X, ops.Z]):
        # Define the number of rounds as 1
        number_of_rounds = 1
        # Start with the iteration number 0
        iteration = 0
        # While the iteration is lower that the one we wish to obtain (success_iterations)
        while iteration < success_iterations:
            q_alice, = qapi.create_qubits(1)  # Create Alice's qubit
            outcome_a = random.randint(0, 1)  # Choose at random the state
            # If we get 1, then we prepare the qubit in |1>
            if outcome_a == 1:
                qapi.operate(q_alice, ops.X)
            # If the chose basis is X shall bring Alice qubit to the X basis
            if basis_choice == "X":
                qapi.operate(q_alice, ops.H)
            # Get Bob's state after the repeater chain
            q_bob = tree_code_repeater_chain(q_alice, num_of_links, branching_vector, loss_probability, depolar_rate)
            # If the process was unsuccessful we add a 1 to the number of rounds
            if q_bob is None:
                number_of_rounds += 1
                continue
            outcome_b, prob = qapi.measure(q_bob, measurement_operator)  # Measure Bob's qubit
            data["basis_A"].append(basis_choice)
            data["basis_B"].append(basis_choice)
            data["outcome_A"].append(outcome_a)
            data["outcome_B"].append(outcome_b)
            data["number_of_rounds"].append(number_of_rounds)
            data["midpoint_outcome_0"].append(0)
            number_of_rounds = 1  # Reset the number of rounds
            iteration += 1  # Add a 1 to the iteration counter

    # Transform data dictionary to a RepchainDataFrameHolder with 2 as number of nodes since it is a one-way quantum
    # repeater
    dataframe = RepchainDataFrameHolder(data=data, number_of_nodes=2, baseline_parameters=baseline_parameters)
    # Define the name of the pickle file where the raw data will be saved
    name_file = '/qkd_{}_links_{}_bv_{}_lp_{}_dp.pickle'.format(num_of_links, branching_vector, loss_probability,
                                                                depolar_rate)
    final_name = path + name_file
    outfile = open(final_name, 'wb')  # Create a pickle file in the path specified and the name defined
    pickle.dump(dataframe, outfile)  # Raw data stored in the pickle file
    return


def _r_successful_z(branching_vector, k, loss_probability):
    """This function computes the probability of having a successful indirect Z measurement of any given qubit in a
    certain level of the tree.
    For more information please refer to the following paper: https://arxiv.org/abs/1907.05101
    Specifically, this equation is equivalent to equation number 6 in the paper.

    Parameters
    ----------
    branching_vector : list of int
    k : int
        This parameter tells which is the level in which we are computing the probability
    loss_probability : number from 0 to 1
        Probability to lose a qubit

    Return
    -----
    r : value from 0 to 1
        Probability of having a successful indirect Z measurement of any given qubit in a certain level of the tree
    """
    d = len(branching_vector)
    # Create a copy of the branching vector so we can later on modified without perturbing the branching vector itself
    m_branching_vector = copy.deepcopy(branching_vector)
    if k > d:
        r_k = 0
        return r_k
    else:
        m_branching_vector.append(0)
        m_branching_vector.append(0)
        r_k_2 = _r_successful_z(branching_vector, k + 2, loss_probability)
        r_kk = (1 - (1 - loss_probability) *
                (1 - loss_probability + loss_probability * r_k_2) ** m_branching_vector[k+1]) ** m_branching_vector[k]
        r_k = 1 - r_kk
        return r_k


def analytical_success_probability(branching_vector, loss_probability):
    """This function computes the success probability of a one-way quantum repeater that uses the tree code as encoding.
    To compute it uses an analytical expression that can be found in the paper listed below under "References".
    Specifically, this equation is equivalent to equation number 5 in the paper.

    References
    ----------
    "One-Way Quantum Repeater Based on Near-Deterministic Photon-Emitter Interfaces",
    https://arxiv.org/abs/1907.05101. Studies the use of tree codes to protect against losses in quantum networks.

    Parameters
    ----------
    branching_vector : list of int
    loss_probability : number from 0 to 1
        Probability to loose a qubit

    Return
    ------
    n : value from 0 to 1
        Success probability of a one-way quantum repeater that uses the tree code as encoding

    """
    r_1 = _r_successful_z(branching_vector, 1, loss_probability)
    r_2 = _r_successful_z(branching_vector, 2, loss_probability)
    n = ((1 - loss_probability + loss_probability*r_1)**branching_vector[0] -
         (loss_probability*r_1)**branching_vector[0]) * \
        (1 - loss_probability + loss_probability*r_2)**branching_vector[1]
    return n
