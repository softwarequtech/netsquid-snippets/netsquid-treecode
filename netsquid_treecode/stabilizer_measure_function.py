import numpy as np
import netsquid.qubits.qubitapi as qapi
from netsquid.qubits import operators as ops


def many_qubit_stab_meas(qubits, stab):
    """ This function takes as an input many physical qubits and applies a stabilizer measurement
    between them using an ancilla.

    The possible stabilizer measurements are combinations of Xs and Zs.
    Remember that for X parts we do H gates before and after a CNOT gate between the qubit and the ancilla.
    For the Z parts we only do a CNOT gate between the qubit and the ancilla.

    Parameters
    ----------
    qubits : list of :class:`netsquid.qubits.qubit.Qubit`
    stab : list of same length as qubits composed by 0s and 1s
        The stabilizer that is measured is P_0P_1P_2...P_n, where P_i is X on qubit `qubits[i]` if `stab[i]` is 0,
        and Z on qubit `qubits[i]` if `stab[i]` is 1.
        For example, two qubits 'qubits = [q0, q1]', 'stab = [a, b]'
        '[a, b]` where a = 0 --> X on q0
                       a = 1 --> Z on q0
                       b = 0 --> X on q1
                       b = 1 --> Z on q1

    Returns
    -------
    m0 : +1 or -1
        Result from the measurement of the ancilla qubit in the stabilizer measurement.

    """
    # First we need to create the ancilla qubit (we just need one as we only perform one stabilizer measurement)
    a0, = qapi.create_qubits(1, 'Ancilla')
    for i, qubit in zip(stab, qubits):
        if i == 0:
            qapi.operate(qubit, ops.H)
        qapi.operate([qubit, a0], ops.CNOT)
        if i == 0:
            qapi.operate(qubit, ops.H)
    m0, prob = qapi.measure(a0)  # Measure ancilla qubit
    m0 = (-1) ** m0  # Make the output of the measurement +/- 1
    return m0


def suc_many_tree_z_z(trees, indices):
    """ This function takes as an input many trees with encoded information and applies a Z...Z stabilizer measurement
    between them. Which, at physical level for a single tree, translates into measuring X on a first-level qubit and,
    and if the tree is depth 2 or more we need to measure in Z basis the second-level qubits of that first-level qubit.
    Also, after those measurements corrections need to be applied, so that if the outcomes of the Z measurements in
    the low-level qubits in a subtree give -1, then Z operation needs to be applied to the first-level qubit of that
    subtree. This is the case that the attempt of a stabilizer measurement between many first level qubits is successful
    so then we measure the second-level qubits in Z.

    References
    ----------
    "Towards a fault-tolerant one-way quantum repeater",
    https://repository.tudelft.nl/islandora/object/uuid:965b09c7-c652-40d5-8f14-2eea99f99983.
    Thesis that studies the concatenation of tree codes with a five-qubit code to protect against both losses and errors
    in quantum networks. Also contains a detailed background section that explains the tree code.

    Parameters
    ----------
    trees : list of :class:`netsquid_treecode.tree_code.Tree`
    indices: list of int
        Indices of the first first-level qubits available in each tree. These indicate which subtrees will be used in
        the stabilizer measurement.

    Returns
    -------
    mx : +1 or-1
        Result from the Z...Z stabilizer measurement between many trees.
    mc : list of the same length as the number of trees that contains +1 or -1
        Result from the Z measurement on the first-level qubit to detach it in each tree

    """
    # We perform a X...X stabilizer measurement between the two first-level qubits
    qubits = [tree.get_qubit([index]) for tree, index in zip(trees, indices)]
    mx = many_qubit_stab_meas(qubits, [0] * len(qubits))
    # Create a list of lists with a 1 where we will store the Z measurements of the second-level qubits if they exists.
    # We add a 1 as a dummy variable, since later it won't affect the outcome
    mz_list = [[] for _ in range(len(qubits))]
    # Measure all the second-level qubits and the ones below them by a Z measurement in each tree
    for tree, index, mz_sub in zip(trees, indices, mz_list):
        if len(tree.branching_vector) >= 2:  # Check if we have second-level qubits or more
            for k in range(tree.branching_vector[1]):
                mz_i = tree.measure_z_qubit([index, k])
                if mz_i is not None:
                    # If the measurement was successful, save the outcome
                    mz_sub.append(mz_i)
    # Make the product of all the outcomes and save them in a list
    mz_prod = [np.prod(mz_sub) for mz_sub in mz_list]
    # The result of the stabilizer is the product of all the measurements
    mx = mx * np.prod(np.array(mz_prod))
    # Perform corrections, if the outcomes of the Z measurements in a subtree give -1, then Z needs to be applied to the
    # first level qubit of that subtree
    for prod, qubit in zip(mz_prod, qubits):
        if prod == -1:
            qapi.operate(qubit, ops.Z)
    # To 'detach' the qubits where we have performed the stabilizer measurement using an ancilla we measure in Z
    mc = []
    for correction, qubit, tree, index in zip(mc, qubits, trees, indices):
        mc_i, prob = qapi.measure(qubit)
        mc_i = (-1) ** mc_i  # Make the outcome +/-1
        mc.append(mc_i)
        tree.set_qubit_used([index])  # Indicate that the qubit has been measure out
    return mx, mc


def many_tree_z_z_stab_meas(trees, hs_outcomes):
    """ This function takes as an input many trees with encoded information and applies a Z...Z stabilizer measurement
    between them. Which, at physical level for a single tree, translates into measuring X on a first-level qubit,
    and if the tree is depth 2 or more we need to measure in Z basis the second-level qubits of that first-level qubit.
    So we will first attempt a X...X stabilizer measurement between many first-level qubits,
        . If it succeeds we measure the second-level qubits in Z
        . If it doesn't succeed we measure the second-level qubits in X, and if there was one of the first-level
            qubits, we would measure it in Z
    This function includes all corrections that need to be applied after performing measurements on trees.

    References
    ----------
    "Towards a fault-tolerant one-way quantum repeater",
    https://repository.tudelft.nl/islandora/object/uuid:965b09c7-c652-40d5-8f14-2eea99f99983.
    Thesis that studies the concatenation of tree codes with a five-qubit code to protect against both losses and errors
    in quantum networks. Also contains a detailed background section that explains the tree code.

    Parameters
    ----------
    trees : list of :class:`netsquid_treecode.tree_code.Tree`
    hs_outcomes : list of list
        Each list inside must contain two elements,
        [['hs_index_0', hs_qubit_0], ..., ['hs_index_n, 'hs_qubit_n']]
        where 'hs_index_i':
            int that indicates the subtree where the Heralded Storage has been successful in the i-th tree
        and 'hs_qubit_i':
            :class:`netsquid.qubits.qubit.Qubit`, which is the qubit that after the Heralded Storage has the information
             for the i-th tree.
        In general this lists are the outcome of 'tree.heralded_storage()'

    Returns
    -------
    mx : +1, -1 or None
        Result from the Z...Z stabilizer measurement between many trees. If it's None means that the stabilizer
        measurement has not been performed due to losses or not available qubits

    """
    # First we check in all the trees which is the first first-level qubit that is available
    # We assume that if any qubit in a tree has already been measured out will be None
    indices = [tree.find_first_level_sub_tree() for tree in trees]
    mx = None  # Initially we set our return variable to None
    if None in indices:
        # If there is no first-level qubit available we also return None
        return mx
    # Now we have 2 options, all first-level qubits exist or some or none exist
    q_states = [(tree.get_qubit([index])).qstate for tree, index in zip(trees, indices)]
    # q_states = [qubit.qstate for qubit in qubits]
    if None not in q_states:
        # If all first-level qubits are not lost we perform the stabilizer measurement
        mx, mc = suc_many_tree_z_z(trees, indices)
        # If the outcomes of the Z measurements on the first-level qubits are -1 a Z operation on the corresponding tree
        # needs to be applied.
        for mc_i, tree, hs_outcome in zip(mc, trees, hs_outcomes):
            if mc_i == -1:
                tree.z_on_tree_using_heralded_storage_subtree(hs_outcome[0], hs_outcome[1])
    else:
        # If any of the first-level qubits are lost we need to detach them from the tree
        for tree, index, hs_outcome in zip(trees, indices, hs_outcomes):
            result = tree.measure_z_qubit([index])  # Perform decoding measurements on tree
            if result == -1:
                # Perform correction, that is Z to the tree, by operating in the Heralded Storage subtree in tree
                tree.z_on_tree_using_heralded_storage_subtree(hs_outcome[0], hs_outcome[1])
    return mx
