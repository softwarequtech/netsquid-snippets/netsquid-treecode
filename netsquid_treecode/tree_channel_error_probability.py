"""
This code is meant to determine the error probability of the following procedure:

1. Encode a qubit in a tree code.

2. Apply depolarizing and pure loss channels to all physical qubits in the code.

3. Try to decode the logical qubit.

We determine the success probability of this procedure (i.e., the probability that decoding works).
Additionally, we define the "tree channel" to be the quantum channel that describes what happens to the qubit
when applying the above procedure, conditioned on successful decoding.

Our main goal is to test the assumption that the tree channel is well-approximated by a depolarizing channel with
an error probability that is three times the error probability applied to the physical qubits in the code. We do this by

1. Estimate the average fidelity of the channel to the trivial channel (identity).
   Under the assumption that the channel is depolarizing, we can translate this into an error probability.

2. Estimate the average fidelity of the channel to a depolarizing channel with the error probability derived at step 1.
   This fidelity is a measure how much the channel looks like a depolarizing channel.

"""
import warnings

import netsquid as ns
import netsquid.qubits.qubitapi as qapi
from netsquid.qubits.ketstates import b00
from netsquid_treecode.tree_code import Tree
from netsquid_treecode.models import PureLossModel
from netsquid.components.models.qerrormodels import DepolarNoiseModel
import time
import pandas as pd
import numpy as np
import os
import itertools
import ast
import multiprocessing as mp
from functools import partial


def send_through_tree_channel(qubit, branching_vector, loss_probability, depolar_probability):
    """Attempt to send a qubit through the "tree channel".

    The action of the tree channel is defined by the following procedure:
    1. Encode a qubit in a tree code.
    2. Apply depolarizing and pure loss channels to all physical qubits in the code.
    3. Try to decode the logical qubit.
    There is some probability that decoding fails. The tree channel is conditioned on the procedure succeeding.
    (So the tree channel describes the output qubit of the above procedure in case decoding succeeds.)

    Parameters
    ----------
    qubit : :class:`netsquid.qubits.qubit.Qubit`
        Qubit which should be attempted to send through the tree channel.
    branching_vector : list of int
        Branching vector characterizing the tree code.
    loss_probability : float
        Probability that a physical qubit is lost. Should be between 0 and 1.
    depolar_probability : float
        Probability that a physical qubit is depolarized. Should be between 0 and 1.

    Returns
    -------
    :class:`netsquid.qubits.qubit.Qubit` or None
        Decoded qubit. None if decoding failed.

    """
    tree = Tree(data_qubit=qubit, branching_vector=branching_vector)
    loss_noise_model = PureLossModel(loss_probability) + DepolarNoiseModel(depolar_probability, time_independent=True)
    loss_noise_model(tree.qubits)
    return tree.store_and_decode_tree()


def determine_choi_state(branching_vector, loss_probability, depolar_probability):
    """Determine the Choi state of the tree channel.

    The "tree channel" is defined in the documentation of :func:`send_through_tree_channel()`.
    The Choi state of a single-qubit channel is the state obtained by applying the channel to one of the qubits
    in the maximally-entangled state (|00> + |11>) / sqrt(2).

    This function applies the encoding / loss + error / decoding procedure once to a qubit in a maximally-mixed
    state and returns both qubits.

    Parameters
    ----------
    branching_vector : list of int
        Branching vector characterizing the tree code.
    loss_probability : float
        Probability that a physical qubit is lost. Should be between 0 and 1.
    depolar_probability : float
        Probability that a physical qubit is depolarized. Should be between 0 and 1.

    Returns
    -------
    :class:`netsquid.qubits.qubit.Qubit`
        Qubit in maximally entangled state that is not acted on.
    :class:`netsquid.qubits.qubit.Qubit` or None
        Qubit in maximally entangled state that is acted on.
        If decoding the tree failed, it will be None instead.

    Note
    ----
    If a quantum-state formalism is used that does not support mixed states, this function applies a Monte Carlo
    simulation. The full Choi state of the channel can then be estimated by calling the function many times and
    creating a mixed state from the outputs.

    """
    # create maximally entangled state 1/sqrt(2) (|00> + |11>)
    q1, q2 = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate([q1, q2], b00)
    q2 = send_through_tree_channel(qubit=q2, branching_vector=branching_vector, loss_probability=loss_probability,
                                   depolar_probability=depolar_probability)
    return q1, q2


def collect_choi_states(branching_vector, loss_probability, depolar_probability, number_of_states, filename):
    """Sample the Choi state of the tree channel multiple times and store the data.

    Data is stored as a csv file.

    Parameters
    ----------
    branching_vector : list of int
        Branching vector characterizing the tree code.
    loss_probability : float
        Probability that a physical qubit is lost. Should be between 0 and 1.
    depolar_probability : float
        Probability that a physical qubit is depolarized. Should be between 0 and 1.
    number_of_states : int
        Number of samples to collect.
    filename : str
        Filename to use to save the data. Recommended to contain the .csv suffix.

    """
    data = {"choi_state": [], "computation_time": []}

    for _ in range(number_of_states):
        tic = time.perf_counter()
        q1, q2 = determine_choi_state(branching_vector=branching_vector, loss_probability=loss_probability,
                                      depolar_probability=depolar_probability)
        toc = time.perf_counter()
        if q2 is not None:
            choi_state = qapi.reduced_dm([q1, q2])
        else:
            choi_state = None
        data["choi_state"].append(choi_state)
        data["computation_time"].append(toc - tic)

    dataframe = pd.DataFrame(data=data)

    # add info about parameters used
    for i in range(len(branching_vector)):
        dataframe[f"b{i}"] = branching_vector[i]
    dataframe["loss_prob"] = loss_probability
    dataframe["depolar_prob"] = depolar_probability

    # reorder columns in dataframe to have parameters first, results second
    cols = [f"b{i}" for i in range(len(branching_vector))]
    cols += ["loss_prob", "depolar_prob", "choi_state", "computation_time"]
    dataframe = dataframe[cols]

    # save dataframe
    dataframe.to_csv(filename, index=False)


def channel_fidelity(choi_state):
    """Calculate the average channel fidelity from the Choi state of that fidelity.

    Parameters
    ----------
    choi_state : :class:`~netsquid.qubits.qrepr.QRepr`, :class:`numpy.array`
        Representation of the Choi state of the channel for which the average fidelity to a depolarizing channel
        should be calculated. Should be a two-qubit state.

    Returns
    -------
    float
        Average fidelity of the channel defined by the Choi state to the identity channel.

    """
    if choi_state is None:
        return None
    choi_qubits = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate(choi_qubits, choi_state)
    choi_state_fidelity = qapi.fidelity(choi_qubits, b00, squared=True)
    channel_fidelity = choi_state_fidelity_to_channel_fidelity(choi_state_fidelity)
    return channel_fidelity


def choi_state_fidelity_to_channel_fidelity(choi_state_fidelity):
    """Calculate the average channel fidelity from the fidelity of the Choi state to a maximally entangled state.

    Only for 2-dimensional channels (so 4-dimensional Choi states).
    """
    return (2 * choi_state_fidelity + 1) / 3


def channel_fidelity_to_depolar_prob(channel_fidelity, error):
    """Calculate the depolarizing probability of a depolarizing channel from the average fidelity of that channel."""
    return 2 * (1 - channel_fidelity), 2 * error


def fidelity_to_depolarizing_channel(choi_state, depolar_prob):
    """Calculate the average fidelity of a channel to a depolarizing channel from the Choi state.

    Parameters
    ----------
    choi_state : :class:`~netsquid.qubits.qrepr.QRepr`, :class:`numpy.array`
        Representation of the Choi state of the channel for which the average fidelity to a depolarizing channel
        should be calculated. Should be a two-qubit state.
    depolar_prob : float
        Deplarizing probability defining the depolarizing channel to which the fidelity should be calculated.

    Returns
    -------
    float
        Fidelity of the channel to a depolarizing channel.

    """
    if depolar_prob > 1. and np.isclose(depolar_prob, 1.):
        depolar_prob = 1.
    choi_qubits = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate(choi_qubits, choi_state)
    depolarized_qubits = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate(depolarized_qubits, b00)
    qapi.depolarize(qubit=depolarized_qubits[1], prob=depolar_prob)
    depolarized_state = qapi.reduced_dm(depolarized_qubits)
    choi_state_fidelity_to_depolar_state = qapi.fidelity(choi_qubits, depolarized_state, squared=True)
    channel_fidelity_to_depolar_channel = choi_state_fidelity_to_channel_fidelity(choi_state_fidelity_to_depolar_state)
    return channel_fidelity_to_depolar_channel


def process_collected_choi_states(choi_states):
    """Calculate success probability, depolarizing probability and fidelity to depolarizing channel from choi states.

    Use an ensemble of sampled Choi states to calculate properties of the channel to which the Choi states correspond.
    The "true" Choi state of the full channel is estimated as a mixture of the sampled Choi states.

    Parameters
    ----------
    choi_states : list
        List of representations of sampled Choi states for the channel. Should be two-qubit states.
        A Choi state should be None in case of heralded failure.

    Returns
    -------
    success_prob : float
        Estimated success probability of the quantum channel.
    success_prob_error : float
        Standard error in the estimate of the success probability.
    depolar_prob : float, None
        Estimated depolarizing probability corresponding to the quantum channel.
        None if the data contains no successes.
    deplar_prob_error : float, None
        Standard error in the estimate of the depolarizing probability.
        None if the data contains no successes.
    fid_to_depolar_channel : float, None
        Estimated average fidelity of the channel to a depolarizing channel.
        None if the data contains no successes.

    """
    qformalism = ns.get_qstate_formalism()
    if qformalism is not ns.QFormalism.DM:
        ns.set_qstate_formalism(ns.QFormalism.DM)

    def success(choi_state):
        """Function that maps the presence/absence of a Choi state to success/failure of decoding the tree state."""
        if choi_state is None:
            return 0
        return 1

    # determine success probability and its standard error
    successes = [success(choi_state) for choi_state in choi_states]
    success_prob = np.average(successes)
    success_prob_error = np.std(successes) / np.sqrt(len(successes))

    if success_prob == 0.:
        return success_prob, success_prob_error, None, None, None

    # determine average fidelity of channel and its standard error
    fids = [channel_fidelity(choi_state) for choi_state in choi_states if choi_state is not None]
    # fids = [f for f in fids if f is not None]
    avg_f = np.average(fids)
    error_f = np.std(fids) / np.sqrt(len(fids))

    # determine depolarizing probability of channel and its error
    depolar_prob, depolar_prob_error = channel_fidelity_to_depolar_prob(avg_f, error_f)

    if depolar_prob > 1.:
        depolar_prob = 1.
        depolar_prob_error = 0.
        warnings.warn(f"\n\nThe channel was found to have an average fidelity of {avg_f}, "
                      f"which is not possible for a depolarizing channel (it has at least fidelity 1/2).\n"
                      f"This indicates that the channel may not be well-approximated by a depolarizing channel, "
                      f"or that not enough data has been collected.\n"
                      f"In the rest of the function, the channel will be compared to a depolarizing channel with "
                      f"with depolarizing probability 1, as that realizes the lowest fidelity.\n"
                      f"The error in the estimate of the depolarizing probability has been set to zero, "
                      f"as it is unclear how the error should be calculated in this case.\n\n")

    # determine average fidelity of channel to depolarizing channel
    choi_states = [choi_state for choi_state in choi_states if choi_state is not None]
    mixed_choi_state = sum(choi_states) / len(choi_states)
    fid_to_depolar_channel = fidelity_to_depolarizing_channel(choi_state=mixed_choi_state, depolar_prob=depolar_prob)

    if qformalism is not ns.QFormalism.DM:  # reset to previous formalism
        ns.set_qstate_formalism(qformalism)

    return success_prob, success_prob_error, depolar_prob, depolar_prob_error, fid_to_depolar_channel


def combine_choi_state_data(path, suffix=".csv", output_filename="combined_choi_state_data.csv"):
    """Combine different data files with choi states into a single big file.

    Parameters
    ----------
    path : str
        Path containing the data files to be combined.
    suffix : str
        Suffix shared by all the files in `path` that should be combined. (Files without the suffix are skipped.)
    output_filename : str
        Filename used to save the combined data.

    """

    n = 0
    df = pd.DataFrame()
    if not os.path.exists(path):
        raise NotADirectoryError(f"The path {path} does not exist.")
    for filename in os.listdir(path):
        if filename[-len(suffix):] == suffix:
            if os.path.isfile("{}/{}".format(path, filename)):
                n += 1
                df = pd.concat([df, pd.read_csv(f"{path}/{filename}")], ignore_index=True)
    df.to_csv(output_filename, index=False)
    print(f"Combined {n} csv files.")


def str2array(string):
    """Convert string expression of numpy array (density matrix) to a numpy array object.

    When putting a numpy array into a pandas `DataFrame`, using the `DataFrame.to_csv()` method,
    and then using `pandas.read_csv()` to retrieve the `DataFrame` again, the numpy array will have been
    converted to a string. This function is able to convert that string back to a numpy array again.

    Parameters
    ----------
    string : str
        String representing the numpy array.

    Returns
    -------
    `numpy.ndarray`
        Array recovered from string.

    """
    if string is None or string == "":
        return string
    return np.array(ast.literal_eval(string.replace("\n", "")
                                     .replace("[ ", "[")
                                     .replace(" +", "+")
                                     .replace("   ", " ")
                                     .replace("  ", " ")
                                     .replace(" ", ", ")))


def load_choi_state_data(filename):
    """Load a file with data on Choi states as a dataframe.

    Parameters
    ----------
    filename : str
        Name of the file to be loaded.

    """
    df = pd.read_csv(filename)
    df = df.fillna(np.nan).replace([np.nan], [None])
    df["choi_state"] = df["choi_state"].apply(str2array)
    return df


def process_choi_state_data(data_filename, output_filename):
    """Process data on Choi states.

    Processes the data and saves the result as a csv file.
    The csv file contains one row for each unique value of the varied parameters in the data
    (i.e., all columns included in the data that are not Choi states and computation times),
    containing the values of the varied parameters and the results of processing.
    The processed data contains all the quantities calculated by :func:`process_collected_choi_states()`
    and the total computation time needed to generate the processed data corresponding to these values of the
    varied parameters.

    Parameters
    ----------
    data_filename : str
        Filename of data file to be processed.
    output_filename : str
        Filename used to save the processed data.

    """
    ns.set_qstate_formalism(ns.QFormalism.DM)  # works best for processing
    raw_data = load_choi_state_data(data_filename)
    varied_params = list(raw_data.columns)
    varied_params.remove("choi_state")
    varied_params.remove("computation_time")
    varied_params_unique_values = []
    for varied_param in varied_params:
        varied_params_unique_values.append(raw_data[varied_param].unique().tolist())

    processed_data = pd.DataFrame()
    for values in itertools.product(*varied_params_unique_values):
        varied_params_with_values = {}
        df = raw_data
        for (varied_param, value) in zip(varied_params, values):
            varied_params_with_values.update({varied_param: value})
            df = df[df[varied_param] == value]
        df.drop(columns=varied_params)

        if df.empty:
            continue

        computation_times = list(df["computation_time"])
        total_computation_time = sum(computation_times)

        choi_states = list(df["choi_state"])
        success_prob, success_prob_error, depolar_prob, depolar_prob_error, fid_to_depolar_channel \
            = process_collected_choi_states(choi_states)

        processed_data_for_these_parameters = {"success_prob": [success_prob],
                                               "success_prob_error": [success_prob_error],
                                               "reencoding_error_prob": [depolar_prob],
                                               "reencoding_error_prob_error": [depolar_prob_error],
                                               "fidelity_to_depolar_channel": [fid_to_depolar_channel],
                                               "computation_time": [total_computation_time],
                                               }
        varied_params_with_values.update(processed_data_for_these_parameters)
        processed_data_for_these_parameters = varied_params_with_values
        processed_data = pd.concat([processed_data, pd.DataFrame(processed_data_for_these_parameters)],
                                   ignore_index=True)
    processed_data.to_csv(output_filename, index=False)


def fiber_length_from_loss_probability(loss_probability):
    """Assumes attenuation length of 20 km.

    Parameters
    ----------
    loss_probability : float
        Probability a photon gets lost in the fiber segment.

    Returns
    -------
    float
        Fiber length in kilometers over which the attenuation loss corresponds to `loss_probability`,
        if the attenuation loss in fiber is 20 km.

    """
    return - 20 * np.log(1 - loss_probability)


def add_fiber_length_to_processed_data(filename):
    """Add the column L_0[km] that contains fiber lengths to a .csv file with processed data.

    This function assumes an attenuation length of 20 km.

    Parameters
    ----------
    filename : str
        Filename of processed data. File will be overwritten.

    """
    df = pd.read_csv(filename)
    df["L_0[km]"] = df["loss_prob"].apply(fiber_length_from_loss_probability)
    df.to_csv(filename, index=False)


def _collect_choi_states_from_row(row_tuple, number_of_states, path):
    """Use a pandas row with information about what data to collect.

    Parameters
    ----------
    row_tuple : tuple
        row_tuple[0] is the index, row_tuple[1] is the actual row.
    number_of_states : int
        How many Choi states to sample.
    path : str
        Path where the collected data is saved.

    """
    row = row_tuple[1]
    fiber_length = row["L_0[km]"]
    loss_probability = 1 - np.exp(- fiber_length / 20)  # assuming attenuation length of 20 km
    depolar_probability = row["physical_error_prob"]
    branching_vector = []
    for i in range(len(row)):
        name = f"b{i}"
        if name in row:
            branching_vector.append(int(row[name]))
    bv_string = ""
    for i in branching_vector:
        bv_string += f"{i}-"
    bv_string = bv_string[:-1]
    filename = f"/{row.name}_{bv_string}_loss_{loss_probability:.2f}_error_{depolar_probability:.2f}_raw_data.csv"
    collect_choi_states(branching_vector=branching_vector, loss_probability=loss_probability,
                        depolar_probability=depolar_probability, number_of_states=number_of_states,
                        filename=path + filename)


def collect_choi_states_from_csv(csv_specification_filename, number_of_states, path, multiprocessing=False):
    """Read a csv file and collect data for the parameters indicated there. Then combine and process the data.

    Every row of the csv file becomes a single Choi-state sample, with the size given by `number_of_states`.
    Note: the csv file should indicate the fiber length instead of the loss probability.
    The loss probability is calculated from this value assuming an attenuation length of 20 km.

    This function uses multiprocessing; the different rows of the file are distributed among different processes.
    The number of used processes is the number of CPU's minus 2.

    Parameters
    ----------
    csv_specification_filename : str
        Filename of the csv file that specifies for what parameters to collect data.
    number_of_states : int
        How many Choi states to sample.
    path : str
        Path where the collected data, combined data and processed data is saved.
    multiprocessing : bool (optional)
        Set True to use all the computer's processors except for two.
        If False, only one will be used.

    """
    if not os.path.isdir(path):
        os.mkdir(path)
    df = pd.read_csv(csv_specification_filename)
    df.reset_index()
    collect_choi_states_fct = partial(_collect_choi_states_from_row, number_of_states=int(number_of_states), path=path)
    if multiprocessing:
        num_processes = max(mp.cpu_count() - 2, 1)
    else:
        num_processes = 1
    pool = mp.Pool(num_processes)
    pool.map(collect_choi_states_fct, df.iterrows())
    pool.close()
    pool.join()
    combine_choi_state_data(path=path, output_filename=path + "/combined_data.csv", suffix="_raw_data.csv")
    process_choi_state_data(data_filename=path + "/combined_data.csv", output_filename=path + "/processed_data.csv")
    add_fiber_length_to_processed_data(path + "/processed_data.csv")
