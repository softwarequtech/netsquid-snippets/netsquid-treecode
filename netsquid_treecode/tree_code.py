import netsquid.qubits.qubitapi as qapi
from netsquid.qubits import operators as ops
import numpy as np


def majority_voting(outcomes):
    """ This function takes a list of +1 and -1 and performs a majority voting. This is as follows:
    If the sum of all the list elements is negative, the majority voting tell us that the right value is -1.
    If the sum is positive the returned value is +1.
    Else, majority voting is inconclusive and `None` is returned.

    Parameters
    ----------
    outcomes : list containing +1 and -1
        The outcomes on which majority voting is performed.

    Returns
    -------
    result : +1, -1 or None
        Returns None when the sum is 0 means that we have the same amount of +1 and -1. So we consider the majority
        voting fails.

    """
    if not outcomes:
        return 1
    m_v = sum(outcomes)  # We sum all the outcomes (+1 or -1) in a subtree
    if m_v < 0:  # If the sum has a negative result means that most of the outcomes were -1
        result = -1
    elif m_v > 0:  # If the sum has a negative result means that most of the outcomes were +1
        result = 1
    else:
        # If the sum is 0 means that we have the same amount of +1 and -1.
        # Then we just consider the majority voting fails, so we return None
        result = None
    return result


def flatten_list(nested_list):
    """Flatten a nested list.

    This function unpacks sub-lists recursively until a list is obtained that contains no sub-lists.

    Parameters
    ----------
    nested_list : list
        List that can have items that are themselves lists.

    Returns
    -------
    list
        A list without sub-lists, containing all the items contained by the deepest-level sub-lists of `nested_list`.

    Notes
    -----
    - The order of items is not preserved. For example, a list `[0, [1], 2]` is flattened to `[0, 2, 1]`, not
    `[0, 1, 2]`.
    - Only unpacks sub-lists, no other collections. E.g., if the list contains a set which in turn contains a list,
    the list contained by the set is not unpacked.

    """

    sublists = [item for item in nested_list if isinstance(item, list)]
    if len(sublists) == 0:  # the list is not nested
        return nested_list
    list_without_sublists = [x for x in nested_list if not isinstance(x, list)]
    flat_list = list_without_sublists + [item for sublist in sublists for item in sublist]
    return flatten_list(flat_list)


class Tree:
    r"""Representation of a single logical qubit in the tree code.

    A tree state is a graph state. A graph state is a quantum state represented by a graph,
    the vertices of which represent qubits and the bonds represent an entangling operation between them.
    The graph representing a tree state is a tree graph, characterized by a branching vector
    [b_0, b_1, ..., b_d]. This branching vector specifies the connectivity of the tree as one moves from the root qubit
    (top node) through the d levels of the tree.
    Example of a tree graph with branching vector [2, 3]:

         o
       /   \
      o     o
     /|\   /|\
    o o o o o o

    (Produced using ChatGPT.)

    A stabilizer operator K_i can be associated with each qubit i in the tree, given by Pauli X on qubit i
    and Pauli Z on all the neighboring qubits (i.e., all qubits connected to qubit i in the graph representing the
    state). The tree state is the unique state with eigenvalue +1 for all the stabilizers K_i.

    A tree state can be used to encode a single logical qubit. This can be achieved by performing a
    Bell-state measurement between a data qubit and the root qubit of the tree state, and performing some corrections
    based on the outcome. This results in a logical qubit in the tree code. The branching vector of the tree used
    in the encoding procedure characterizes the tree code.

    References
    ----------
    1. "Loss tolerance in one-way quantum computation via counterfactual error correction",
    https://arxiv.org/abs/quant-ph/0507036. First introduction of the tree code.

    2. "One-Way Quantum Repeater Based on Near-Deterministic Photon-Emitter Interfaces",
    https://arxiv.org/abs/1907.05101. Studies the use of tree codes to protect against losses in quantum networks.

    3. "Towards a fault-tolerant one-way quantum repeater",
    https://repository.tudelft.nl/islandora/object/uuid:965b09c7-c652-40d5-8f14-2eea99f99983.
    Thesis that studies the concatenation of tree codes with a five-qubit code to protect against both losses and errors
    in quantum networks. Also contains a detailed background section that explains the tree code.

    4. "Entanglement in Graph States and its Applications",
    https://arxiv.org/abs/quant-ph/0602096. A good introduction to graph states / cluster states.

    Parameters
    ----------
    data_qubit : :class:`netsquid.qubits.qubit.Qubit`
        Qubit to be encoded in the tree code.
    branching_vector : list of int
        Branching vector characterizing the tree code.

    """

    def __init__(self, data_qubit, branching_vector):
        self._branching_vector = branching_vector
        self._encode_tree(data_qubit=data_qubit, branching_vector=self._branching_vector)
        self._perform_tree_encoding_corrections()

    @property
    def branching_vector(self):
        """Branching vector of the tree state used to encode this qubit."""
        return self._branching_vector

    @property
    def qubits(self):
        """All individual qubits that make up the logical qubits in a flat list."""
        return flatten_list(self._nested_list_of_qubits)

    def get_qubit(self, position_vector):
        r"""Get one of the individual qubits that together make up the logical qubit in the tree code.

        Qubits are referenced using their 'position vector', which is a vector which points to the position they held
        in the tree state used in the encoding procedure.
        As an example, the ASCII art below shows the position vectors of the individual qubits making up a logical
        qubit in the tree code with branching vector [3, 2, 3]. In this example the root qubit is not showing.

                           [0]                   [1]                   [2]
                          /   \                 /   \                 /   \
                  __[0, 0]__   [0, 1]     [1, 0]     [1, 1]     [2, 0]   __[2, 1]__
                 /     |    \ ......................................... /     |    \
        [0, 0, 0]  [0, 0, 1]  [0, 0, 2] ...................... [2, 1, 0]  [2, 1, 1]  [2, 1, 2]

        Parameters
        ----------
        position_vector : list of int
            Vector identifying a single physical qubit in the encoded state.
            References the position of the physical qubit in the tree state that was used in the encoding procedure.
            `[i]` refers to the ith first-level qubit, `[i, j]` refers the the jth leaf of the ith first-level qubit,
            `[i, j, k]` to the kth leaf of that leaf, and so on. Indices start counting at 0.

        Returns
        -------
        :class:`netsquid.qubits.qubit.Qubit` or None
            Returns the qubit, unless when the qubit has been "used" (the status of a qubit can be set to "used"
            by calling :meth:`netsquid_treecode.tree_code.Tree.set_qubit_used`). In that case, None is returned.

        """
        entry = self._get_entry_from_nested_qubits(position_vector)
        qubit = entry[0]
        return qubit

    def set_qubit_used(self, position_vector):
        """Set the status of a qubit in the tree code to "used".

        When a qubit is "used", this typically means it has been measured out.
        This, e.g., happens when performing heralded storage, or performing a stabilizer measurement between two
        tree-encoded qubits.

        Parameters
        ----------
        position_vector : list of int
            Vector identifying a single physical qubit in the encoded state.
            References the position of the physical qubit in the tree state that was used in the encoding procedure.
            `[i]` refers to the ith first-level qubit, `[i, j]` refers the the jth leaf of the ith first-level qubit,
            `[i, j, k]` to the kth leaf of that leaf, and so on. Indices start counting at 0.
            See documentation of :meth:`netsquid_treecode.tree_code.Tree.get_qubit` for visual explanation.

        """
        entry = self._get_entry_from_nested_qubits(position_vector)
        entry[0] = None  # the entry in the nested list that held the qubit now instead holds None

    def find_first_level_sub_tree(self):
        """Find a subtree of which the first-level qubit is still available.

         Returns
         -------
         index : int or None
             int  - Index of the first sub-tree that at least has the first-level qubit not None
             None - In case there is no subtree with at least the top qubit then None is returned

        """
        index = None
        for i in range(self.branching_vector[0]):
            if self.get_qubit([i]) is not None:  # First we check for the first-level qubit
                index = i
                break
        return index

    def _get_entry_from_nested_qubits(self, position_vector):
        """Get list entry of internal nested list used to store qubits corresponding to specific position vector.

        Parameters
        ----------
        position_vector : list of int
            Vector identifying a single physical qubit in the encoded state.
            References the position of the physical qubit in the tree state that was used in the encoding procedure.
            `[i]` refers to the ith first-level qubit, `[i, j]` refers the the jth leaf of the ith first-level qubit,
            `[i, j, k]` to the kth leaf of that leaf, and so on. Indices start counting at 0.
            See documentation of :meth:`netsquid_treecode.tree_code.Tree.get_qubit` for visual explanation.

        Returns
        -------
        entry: list
            Entry from `self._nested_list_of_qubits` defined by `position_vector`.
            `entry[0]` is the qubit defined by `position_vector`,
            `entry[i]` for `i > 0` are the entries corresponding to the subtrees attached to this qubit.

        """
        if len(position_vector) > len(self.branching_vector) + 1:
            raise ValueError(f"Position vector {position_vector} is invalid "
                             f"for branching vector {self.branching_vector}")

        entry = self._nested_list_of_qubits
        for i in position_vector:
            entry = entry[i + 1]
        return entry

    def _create_tree_state(self, branching_vector):
        """Creates a tree state.

       Parameters
       ----------
       branching_vector : list of int
           Branching vector describing the tree.
           `branching_vector[i]` is the number of qubits in layer `i` per qubit in layer `i - 1`.

        Returns
        -------
        list
            Nested lists containing qubits.
            The root qubit can be found under tree[0].
            The ith first-level leaf can be found under tree[i+1][0].
            The jth second-level leaf of the ith first-level leaf can be found under tree[i+1][j+1][0], and so forth.

        """
        [root_qubit] = qapi.create_qubits(1)  # Create the root qubit
        qapi.operate(root_qubit, ops.H)  # Bring it to the |+> state
        # Return the nested list
        return [root_qubit] + self._create_subtrees(root_qubit=root_qubit, branching_vector=branching_vector)

    def _create_subtrees(self, root_qubit, branching_vector):
        """This is a recursive function used for tree creation.
        Creates subtrees, taking as an input a root qubit that can be either the real root qubit of the tree or any
        other qubit in the tree that has leaves, by leaves we mean the qubits below the targeted qubit.
        The created subtrees are stored in a nested list with same structure as the tree list for each subtree, and a
        list of all the nested lists for all the subtrees is returned

        Parameters
        ----------
        root_qubit : :class:`netsquid.qubits.qubit.Qubit`
            Root qubit initialized previously at the |+> state
        branching_vector : list of int
           Branching vector describing the tree.
           `branching_vector[i]` is the number of qubits in layer `i` per qubit in layer `i - 1`.

        Returns
        -------
        list
            List of nested lists containing qubits. Each of the nested lists inside follows the structure of the
            '_create_tree_state' return.

        """
        # Define the number of first level qubits, that is the same that the total number of subtrees in a tree
        number_of_subtrees = branching_vector[0]
        # Each of the subtrees will have a branching vector,
        # that is the same as the one for the original tree without the first number
        branching_vector_each_subtree = branching_vector[1:]
        # Create all the low level qubits
        qubits = qapi.create_qubits(number_of_subtrees)
        for qubit in qubits:
            # Bring all the created qubits to the |+> state
            qapi.operate(qubit, ops.H)
            # Perform a CZ between the root qubit and each of the low level qubits
            qapi.operate([root_qubit, qubit], ops.CZ)
        # If the branching vector of the subtree is deeper re-do the same process
        if branching_vector_each_subtree:
            return [[qubit] + self._create_subtrees(root_qubit=qubit, branching_vector=branching_vector_each_subtree)
                    for qubit in qubits]
        # If the branching vector has reached the bottom qubits
        else:
            return [[qubit] for qubit in qubits]

    def _encode_tree(self, data_qubit, branching_vector):
        """This function takes a data qubit and encodes it in a tree code.

       Parameters
       ----------
       data_qubit : :class:`netsquid.qubits.qubit.Qubit`
           Qubit that contains the information to be encoded in the tree
       branching_vector : list of int
           Branching vector describing the tree.
           `branching_vector[i]` is the number of qubits in layer `i` per qubit in layer `i - 1`.

        Returns
        -------
        encoded_tree : list
            Nested list containing qubits.
            On tree[0] there used to be the root qubit that has been consumed in the Bell State Measurement, and
            has been replaced by None.
            The ith first-level leaf can be found under tree[i+1][0].
            The jth second-level leaf of the ith first-level leaf can be found under tree[i+1][j+1][0], and so forth.
        md : 0 or 1
            Result from the measurement of the data qubit in the Bell State Measurement.
        mr : 0 or 1
            Result from the measurement of the root qubit in the Bell State Measurement.

        """
        tree = self._create_tree_state(branching_vector)  # Create the tree
        root_qubit = tree[0]  # Identify the root qubit
        qapi.operate([data_qubit, root_qubit], ops.CNOT)  # Perform a BSM between the data qubit and the root qubit
        qapi.operate(data_qubit, ops.H)
        md, prob = qapi.measure(data_qubit, discard=True)
        mr, prob = qapi.measure(root_qubit, discard=True)
        tree[0] = None  # delete the root qubit from the tree
        self._nested_list_of_qubits = tree
        self._encoding_measurement_outcome_data_qubit = md
        self._encoding_measurement_outcome_root_qubit = mr

    def measure_z_qubit(self, position_vector):
        """This function performs a direct and indirect Z measurement of the qubit in the position defined by the input
        'position_vector'. This is Z on the qubit directly and X on the qubits in a level below, the latter is done by
        calling the '_measure_x_qubit_and_z_on_its_leaves', which measures the targeted qubit in X and in Z the qubits
        below it, by again calling this function, 'measure_z_qubit'.
        Then the outcomes of measuring X on lower qubits (or more measurements on even lower qubits) and the outcome of
        the direct Z measurement are majority voted.

        Parameters
        ----------
        position_vector : list of int
            Vector identifying a single physical qubit in the encoded state.
            References the position of the physical qubit in the tree state that was used in the encoding procedure.
            `[i]` refers to the ith first-level qubit, `[i, j]` refers the the jth leaf of the ith first-level qubit,
            `[i, j, k]` to the kth leaf of that leaf, and so on. Indices start counting at 0.
            See documentation of :meth:`netsquid_treecode.tree_code.Tree.get_qubit` for visual explanation.

        Returns
        -------
        outcome : +/- 1 or None
            None is returned if no result could be obtained, either because none of the measurements succeeded
            or because there were as many +1 as -1 measurement outcomes, meaning that majority voting is inconclusive.

        """
        outcomes = []  # Create an empty list to store the outcomes
        qubit = self.get_qubit(position_vector)  # Get the targeted qubit
        if qubit is not None and qubit.qstate is not None:
            mz, prob = qapi.measure(qubit, ops.Z)  # If the qubit is not lost measure it in Z basis
            mz = (-1) ** mz  # Make the outcome +/-1
            self.set_qubit_used(position_vector)  # Set the qubit as it has been used
            outcomes.append(mz)  # Put the outcome in list outcomes
        if len(position_vector) < len(self.branching_vector):  # If there are lower qubits
            level = len(position_vector)  # Define in which level of the tree we are
            for i in range(self.branching_vector[level]):
                # Iterate for all the qubits in the specific subtree in the lower level
                position_vector_leaf = position_vector + [i]  # Define the position vector for each of the lower qubits
                mx = self._measure_x_qubit_and_z_on_its_leaves(position_vector_leaf)  # Measure those in X basis
                if mx is not None:
                    # If the outcomes of the lower measurements are successful put them in the list
                    outcomes.append(mx)
        if not outcomes:
            return None
        else:
            outcome = majority_voting(outcomes)  # Perform majority voting of all the outcomes
            return outcome

    def _measure_x_qubit_and_z_on_its_leaves(self, position_vector):
        """This function is used in 'measure_z_qubit' that performs an direct and indirect Z measurement of a qubit.
        This function performs an X measurement in the qubit in the position defined by the input 'position_vector'.
        If there are lower qubits (leaves) attaches to the targeted qubit, those are measured in Z basis calling
        'measure_z_qubit'. The outcomes of all measurements are multiplied.

        Parameters
        ----------
        position_vector : list of int
            Vector identifying a single physical qubit in the encoded state.
            References the position of the physical qubit in the tree state that was used in the encoding procedure.
            `[i]` refers to the ith first-level qubit, `[i, j]` refers the the jth leaf of the ith first-level qubit,
            `[i, j, k]` to the kth leaf of that leaf, and so on. Indices start counting at 0.
            See documentation of :meth:`netsquid_treecode.tree_code.Tree.get_qubit` for visual explanation.

        Returns
        -------
        outcome : +/- 1 or None
            None is returned if not all the measurements are successful.
            That is, both the X measurement on the target and the Z measurements on its leaves need to be successful
            in order to get an outcome of +/- 1 instead of None.

        """
        outcomes = []  # Create an empty list to store the outcomes
        qubit = self.get_qubit(position_vector)  # Get the targeted qubit
        if qubit is not None and qubit.qstate is not None:
            mx, prob = qapi.measure(qubit, ops.X)  # If the qubit is not lost measure it in X basis
            mx = (-1) ** mx  # Make the outcome +/-1
            self.set_qubit_used(position_vector)  # Set the qubit as it has been used
            outcomes.append(mx)  # Put the outcome in list outcomes
        else:
            outcomes.append(None)
        if len(position_vector) < len(self.branching_vector):  # If there are lower qubits
            level = len(position_vector)
            for i in range(self.branching_vector[level]):
                # Iterate for all the qubits in the specific subtree in the lower level
                position_vector_leaf = position_vector + [i]  # Define the position vector for each of the lower qubits
                mz = self.measure_z_qubit(position_vector_leaf)  # Measure those in Z basis
                outcomes.append(mz)  # Put the outcome in list outcomes
        if None in outcomes:
            # If the outcomes list is not complete (meaning that we need both X on the target and Z on the leaves to be
            # successful) we return None
            return None
        else:
            # If there are outcomes make the product of all of them
            outcome = np.prod(outcomes)
            return outcome

    def _perform_tree_encoding_corrections(self):
        """Perform corrections corresponding to outcome of Bell-state measurement in encoding procedure."""
        mr = self._encoding_measurement_outcome_root_qubit
        md = self._encoding_measurement_outcome_data_qubit
        # If the outcome of the measurement of the data qubit in the BSM is 1, we shall perform a correction.
        # Such correction is a logical Z operator in the tree,
        # which at physical level means we need to perform X on a first
        # level qubit and, and if the tree is depth 2 we need to apply Z to the second-level qubits of that first-level
        # qubit
        index = self.find_first_level_sub_tree()

        if md == 1:
            if index is not None:
                qapi.operate(self.get_qubit([index]), ops.X)  # Apply X on the first-level qubit
                if len(self.branching_vector) > 1:  # Check if we have second-level qubits
                    for j in range(self.branching_vector[1]):
                        qubit = self.get_qubit([index, j])
                        if qubit is not None:  # If the qubit exists
                            qapi.operate(qubit, ops.Z)  # Apply Z to the second-level qubits
            else:
                raise ValueError('There are no first level qubits in the tree')
        # If the outcome of the measurement of the root qubit in the BSM is 1, we shall perform a correction.
        # Such correction is a logical X operator in the tree, which at physical level means we need to perform Z on all
        # first-level qubits, for both depth 1 and 2 trees.
        if mr == 1:
            for k in range(self.branching_vector[0]):
                qubit = self.get_qubit([k])
                if qubit is not None:  # Only if the qubit is there
                    qapi.operate(qubit, ops.Z)

    def heralded_storage(self):
        """This function takes an encoded (and corrected) tree and performs the heralded-storage procedure.
        Its main function is to ensure that if a Bell-state measurement is attempted with a lost 1st-level qubit, the
        measurement will abort without perturbing the root qubit.

        References
        ----------
        "One-Way Quantum Repeater Based on Near-Deterministic Photon-Emitter Interfaces",
        https://arxiv.org/abs/1907.05101. Studies the use of tree codes to protect against losses in quantum networks.

        Returns
        -------
        index : int or None
            Indicator of the subtree where the Heralded Storage has been successful (None if unsuccessful)
        storage : :class:`netsquid.qubits.qubit.Qubit`
            Qubit that after the Heralded Storage has the information

        """
        index = None
        storage, = qapi.create_qubits(1)
        qapi.operate(storage, ops.H)
        for i in range(self.branching_vector[0]):
            qubit = self.get_qubit([i])
            if qubit is None:
                continue
            if qubit.qstate is not None:
                qapi.combine_qubits([storage, qubit])
                qapi.operate([qubit, storage], ops.CZ)
                qapi.operate(qubit, ops.H)
                m, prob = qapi.measure(qubit)
                qapi.operate(storage, ops.H)
                if m == 1:
                    qapi.operate(storage, ops.Z)
                index = i
                self.set_qubit_used([i])
                break
        return index, storage

    def z_on_tree_using_heralded_storage_subtree(self, hs_index, hs_qubit):
        """This function performs a Z operation to the whole tree by acting on the Heralded Storage subtree.
        The operation is such that X is performed on the stored qubit and either direct Z on the second-level qubits of
        the subtree where the Heralded Storage was successful or indirect Z on that same qubit by acting on lower level
        qubits.

        Parameters
        ----------
        hs_index : int
            Indicator of the subtree where the Heralded Storage has been successful
        hs_qubit : :class:`netsquid.qubits.qubit.Qubit`
            Qubit that after the Heralded Storage has the information

        Returns
        -------
        success : True or False
            If it is False means that the Heralded Storage subtree has lost some qubits such that the decoding of it is
            no longer possible
        """
        success = True
        qapi.operate(hs_qubit, ops.X)
        if len(self.branching_vector) > 1:
            for o in range(self.branching_vector[1]):
                success = self._z_operation_qubit([hs_index, o])
                if success is False:
                    break
        return success

    def _z_operation_qubit(self, position_vector):
        """This function performs a direct or indirect Z operation on a qubit corresponding to a certain position vector

        Parameters
        ----------
        position_vector : list of int
            Vector identifying a single physical qubit in the encoded state.
            References the position of the physical qubit in the tree state that was used in the encoding procedure.
            `[i]` refers to the ith first-level qubit, `[i, j]` refers the the jth leaf of the ith first-level qubit,
            `[i, j, k]` to the kth leaf of that leaf, and so on. Indices start counting at 0.
            See documentation of :meth:`netsquid_treecode.tree_code.Tree.get_qubit` for visual explanation.

        Returns
        -------
        success : True or False
            If it is False means that the Z operation was not successful neither directly or indirectly

        """
        success = False
        qubit = self.get_qubit(position_vector)
        if qubit is not None and qubit.qstate is not None:
            qapi.operate(qubit, ops.Z)
            success = True
        else:
            if len(position_vector) < len(self.branching_vector):
                level = len(position_vector)
                for i in range(self.branching_vector[level]):
                    position_vector_leaf = position_vector + [i]
                    success = self._indirect_z_operation_qubit(position_vector_leaf)
                    if success:
                        break
        return success

    def _indirect_z_operation_qubit(self, position_vector):
        """This function performs an indirect Z operation on a qubit corresponding to a certain position vector. This
        operation is such that if only one level below is available, the qubits in that level connected to the targeted
        have X applied. If more one more level is available the qubits have Z applied to them.

        Parameters
        ----------
        position_vector : list of int
            Vector identifying a single physical qubit in the encoded state.
            References the position of the physical qubit in the tree state that was used in the encoding procedure.
            `[i]` refers to the ith first-level qubit, `[i, j]` refers the the jth leaf of the ith first-level qubit,
            `[i, j, k]` to the kth leaf of that leaf, and so on. Indices start counting at 0.
            See documentation of :meth:`netsquid_treecode.tree_code.Tree.get_qubit` for visual explanation.

        Returns
        -------
        success : True or False
            If it is False means that the indirect Z operation was not successful neither directly or indirectly

        """
        success = False
        qubit = self.get_qubit(position_vector)
        if qubit is not None and qubit.qstate is not None and len(position_vector) >= len(self.branching_vector):
            qapi.operate(qubit, ops.X)
            success = True
        elif qubit is not None and qubit.qstate is not None and len(position_vector) < len(self.branching_vector):
            qapi.operate(qubit, ops.X)
            level = len(position_vector)
            for i in range(self.branching_vector[level]):
                position_vector_leaf = position_vector + [i]
                success = self._z_operation_qubit(position_vector_leaf)
                if success is False:
                    break
        return success

    def decode_tree(self, index, storage_qubit):
        """This function takes an encoded (and corrected) tree and decodes the tree, taking into account
        the possible losses that may happen after the encoding. This only succeeds if the Heralded Storage was
        successful. The deciding procedure performed includes the Pauli corrections that arise from the measurement
        outcomes in the decoding.

        References
        ----------
        "One-Way Quantum Repeater Based on Near-Deterministic Photon-Emitter Interfaces",
        https://arxiv.org/abs/1907.05101. Studies the use of tree codes to protect against losses in quantum networks.

        Note
        ----
        For most use cases, when a tree-encoded qubit needs to be decoded, it is easier to use the method
        :meth:`store_and_decode_tree` instead of this one. That method automatically also performs the heralded storage.

        Parameters
        ----------
        index : int
            Indicator of the subtree where the Heralded Storage has been successful
        storage_qubit : :class:`netsquid.qubits.qubit.Qubit`
            Qubit that after the Heralded Storage has the information

        Returns
        -------
        success : True or False
                True if the decoding procedure was successful, meaning that the Heralded Storage was successful and that
                in the Heralded Storage subtree there wasn't any loss.
                False if the decoding procedure was unsuccessful, meaning that the Heralded Storage was unsuccessful,
                that in the Heralded Storage subtree there was some loss or that there no other qubits available in the
                tree to get the outcomes to perform the corrections of the decoding.
        """
        success = True
        if index is None:
            success = False
            # This means that the Heralded Storage failed
            return success
        # We create a list with a +1, where we will store the +/- 1 outcomes of the measurement of the other subtrees
        corrections_1 = []
        # We are going to store the +/- 1 in a list of lists, where each list inside are the measurement outcomes
        # of a sub-tree. So that later on can be used to perform majority voting
        for k in range(self.branching_vector[0]):
            if k != index and self.get_qubit([k]) is not None:
                # For each subtree that is not the Heralded Storage subtree we perform the decoding operation and we
                # save the outcomes of it
                outcome = self.measure_z_qubit([k])
                if outcome is not None:
                    corrections_1.append(outcome)
                else:
                    success = False
                    return success
        # We create a list with a +1, where we will store the +/- 1 outcomes of the measurement of the second-level
        # qubits of the 'index' subtree.
        corrections_2 = [1]
        if len(self.branching_vector) > 1:  # Check if there are second-level qubits or more
            for j in range(self.branching_vector[1]):
                outcome = self.measure_z_qubit([index, j])
                if outcome is not None:
                    corrections_2.append(outcome)
                else:
                    # There are no qubits on that subtree to decode the tree
                    success = False
                    return success

        # If the product of all the outcomes in corrections_2 is -1, we need to perform Z on the storage qubit
        if np.prod(np.array(corrections_2)) == -1:
            qapi.operate(storage_qubit, ops.Z)
        # If the final product of all the outcomes in corrections_1 is -1, we need to perform X on the storage qubit
        if np.prod(np.array(corrections_1)) == -1:
            qapi.operate(storage_qubit, ops.X)

        # Now our tree is 'empty' but the 'index' first-level qubit, which will be a|+> + b|-> (the initial data
        # qubit was a|0> + b|1>)
        qapi.operate(storage_qubit, ops.H)  # We bring it to the Z basis
        return success

    def store_and_decode_tree(self):
        """Decode the logical qubit of the tree code into a single physical qubit.

        This function does the following:
        1. Attempt heralded storage on each subtree of the code until it is successful in some subtree.
        2. Measure out all lower-level qubits in that subtree.
        3. Measure out all other subtrees.
        4. Perform corrections on the one remaining qubit in accordance with measurement outcomes.

        If too many qubits in the code were lost and too few measurement results can be obtained,
        the decoding procedure is heralded as a failure.

        Returns
        -------
        decoded_qubit : :class:`netsquid.qubits.qubit.Qubit` or None
            The decoded qubit. None in case of a heralded failure in the decoding procedure.

        """
        successful_subtree_index, decoded_qubit = self.heralded_storage()
        if successful_subtree_index is None:
            return None
        success = self.decode_tree(successful_subtree_index, decoded_qubit)
        if not success:
            return None
        return decoded_qubit
