import numpy as np
import copy
import netsquid.qubits.qubitapi as qapi
import netsquid.qubits.operators as ops
from netsquid_treecode.repeater_chain import tree_code_repeater_chain
from netsquid_treecode.repeater_chain import analytical_success_probability


def test_unconcatenated_owqr(branching_vector=[3, 2]):
    qinit, = qapi.create_qubits(1)
    qapi.operate(qinit, ops.H)
    ref = copy.deepcopy(qinit.qstate.ket)
    outcome = tree_code_repeater_chain(qinit, 3, branching_vector, 0, 0)
    fidelity = qapi.fidelity(outcome, ref)
    assert np.isclose(fidelity, 1)


def test_analytical_success_probability(branching_vector=[3, 3, 4], u=0):
    assert analytical_success_probability(branching_vector, u) == 1


def test_analytical_2(branching_vector=[3, 2], u=0.4):
    r_4 = 0
    r_3 = 0
    r_2 = 1 - (1 - (1 - u) * (1 - u + u * r_4) ** 0) ** 0
    r_1 = 1 - (1 - (1 - u) * (1 - u + u * r_3) ** 0) ** branching_vector[1]
    n = ((1 - u + u*r_1)**3 - (u*r_1)**3)*(1 - u + u*r_2)**2
    a = analytical_success_probability(branching_vector, u)
    assert a == n
