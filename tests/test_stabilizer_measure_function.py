from netsquid_treecode.stabilizer_measure_function import many_qubit_stab_meas
from netsquid_treecode.stabilizer_measure_function import many_tree_z_z_stab_meas
from netsquid_treecode.tree_code import Tree
import netsquid.qubits.operators as ops
import netsquid.qubits.qubitapi as qapi
import netsquid.qubits.ketstates as ketstates
import numpy as np
import copy
import unittest
import netsquid as ns


class TestStabMeas(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ns.set_qstate_formalism(ns.QFormalism.KET)

    def setUp(self):
        self.qubits = qapi.create_qubits(2)

    def test_b00(self):
        """Stabilizers ZZ and XX with phases +1 both."""
        qapi.assign_qstate(self.qubits, ketstates.b00)
        assert many_qubit_stab_meas([self.qubits[0], self.qubits[1]], [1, 1]) == 1
        assert many_qubit_stab_meas([self.qubits[0], self.qubits[1]], [0, 0]) == 1

    def test_b11(self):
        """Stabilizers ZZ and XX with phases -1 both."""
        qapi.assign_qstate(self.qubits, ketstates.b11)
        assert many_qubit_stab_meas([self.qubits[0], self.qubits[1]], [1, 1]) == -1
        assert many_qubit_stab_meas([self.qubits[0], self.qubits[1]], [0, 0]) == -1

    def test_b01(self):
        """Stabilizers ZZ and XX with phases -1 and +1 respectively."""
        qapi.assign_qstate(self.qubits, ketstates.b01)
        assert many_qubit_stab_meas([self.qubits[0], self.qubits[1]], [1, 1]) == -1
        assert many_qubit_stab_meas([self.qubits[0], self.qubits[1]], [0, 0]) == 1

    def test_b10(self):
        """Stabilizers ZZ and XX with phases +1 and -1 respectively."""
        qapi.assign_qstate(self.qubits, ketstates.b10)
        assert many_qubit_stab_meas([self.qubits[0], self.qubits[1]], [1, 1]) == 1
        assert many_qubit_stab_meas([self.qubits[0], self.qubits[1]], [0, 0]) == -1

    def test_graph_state(self):
        """Stabilizers XZ and ZX with phases +1 both."""
        qapi.operate(self.qubits[0], ops.H)
        qapi.operate(self.qubits[1], ops.H)
        qapi.operate(self.qubits, ops.CZ)
        assert many_qubit_stab_meas([self.qubits[0], self.qubits[1]], [0, 1]) == 1
        assert many_qubit_stab_meas([self.qubits[0], self.qubits[1]], [1, 0]) == 1


class TestManyTreeZZStabMeas(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ns.set_qstate_formalism(ns.QFormalism.KET)
        cls.branching_vector = [3, 2]

    def setUp(self):
        self.qubits = qapi.create_qubits(2)

    def encode_in_tree(self):
        self.tree_0 = Tree(self.qubits[0], self.branching_vector)
        self.tree_1 = Tree(self.qubits[1], self.branching_vector)

    def heralded_storage(self):
        self.hs_0 = self.tree_0.heralded_storage()
        self.hs_1 = self.tree_1.heralded_storage()

    def measure_zz_stab_on_trees(self):
        return many_tree_z_z_stab_meas(trees=[self.tree_0, self.tree_1],
                                       hs_outcomes=[self.hs_0, self.hs_1])

    def encode_in_tree_and_measure_zz_stab(self):
        self.encode_in_tree()
        self.heralded_storage()
        return self.measure_zz_stab_on_trees()

    def decode_after_stab_measure(self):
        self.d_tree_0 = self.tree_0.decode_tree(self.hs_0[0], self.hs_0[1])
        self.d_tree_1 = self.tree_1.decode_tree(self.hs_1[0], self.hs_1[1])
        return [self.d_tree_0, self.d_tree_1]

    def decoded_state(self):
        return [self.hs_0[1], self.hs_1[1]]

    def test_b00(self):
        """Stabilizer ZZ phase +1.
           Successful decoding.
           Fidelity of the final state after decoding and the state b00."""
        qapi.assign_qstate(self.qubits, ketstates.b00)
        assert self.encode_in_tree_and_measure_zz_stab() == 1
        assert False not in self.decode_after_stab_measure()
        assert np.isclose(qapi.fidelity(self.decoded_state(), ketstates.b00), 1)

    def test_b11(self):
        """Stabilizer ZZ phase -1.
           Successful decoding.
           Fidelity of the final state after decoding and the state b11."""
        qapi.assign_qstate(self.qubits, ketstates.b11)
        assert self.encode_in_tree_and_measure_zz_stab() == -1
        assert False not in self.decode_after_stab_measure()
        assert np.isclose(qapi.fidelity(self.decoded_state(), ketstates.b11), 1)

    def test_b01(self):
        """Stabilizer ZZ phase -1.
           Successful decoding.
           Fidelity of the final state after decoding and the state b01."""
        qapi.assign_qstate(self.qubits, ketstates.b01)
        assert self.encode_in_tree_and_measure_zz_stab() == -1
        assert False not in self.decode_after_stab_measure()
        assert np.isclose(qapi.fidelity(self.decoded_state(), ketstates.b01), 1)

    def test_b10(self):
        """Stabilizer ZZ phase +1.
           Successful decoding.
           Fidelity of the final state after decoding and the state b10."""
        qapi.assign_qstate(self.qubits, ketstates.b10)
        assert self.encode_in_tree_and_measure_zz_stab() == +1
        assert False not in self.decode_after_stab_measure()
        assert np.isclose(qapi.fidelity(self.decoded_state(), ketstates.b10), 1)

    def test_b00_two_measurements(self):
        """Perform the stabilizer measurement twice. This can fail if qubits are not set to "used" correctly.
           Successful decoding.
           Fidelity of the final state after decoding and the state b10."""
        qapi.assign_qstate(self.qubits, ketstates.b00)
        self.encode_in_tree()
        self.heralded_storage()
        assert self.measure_zz_stab_on_trees() == +1
        assert self.measure_zz_stab_on_trees() == +1
        assert False not in self.decode_after_stab_measure()
        assert np.isclose(qapi.fidelity(self.decoded_state(), ketstates.b00), 1)


def test_stabilizer_measurements_and_final_state_in_bit_flip_code(branching_vector=[3, 2]):
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.STAB)
    # We will test for the bit-flip encoding
    # Create three 'data' qubits
    qd1, qd2, qd3 = qapi.create_qubits(3)

    # Create bit-flip encoded qubit
    qapi.operate(qd1, ops.H)  # Initialize the first data qubit
    qapi.operate([qd1, qd2], ops.CNOT)  # Apply CNOT between first and second 'data' qubits
    qapi.operate([qd1, qd3], ops.CNOT)  # Apply CNOT between first and third 'data' qubits

    # Save the state as teh reference for later
    ref = copy.deepcopy(qd1.qstate.ket)

    # Assert all stabilizers of the bit-flip code give expected results at this point
    z1z2 = many_qubit_stab_meas([qd1, qd2], [1, 1])
    z2z3 = many_qubit_stab_meas([qd2, qd3], [1, 1])
    z1z3 = many_qubit_stab_meas([qd1, qd3], [1, 1])
    assert (z1z2 == z2z3 == z1z3 == 1)

    # Encode and correct each of the 'data' qubits in a tree
    tree_1 = Tree(qd1, branching_vector)
    tree_2 = Tree(qd2, branching_vector)
    tree_3 = Tree(qd3, branching_vector)

    # For each tree, perform heralded storage
    hs_1 = tree_1.heralded_storage()
    hs_2 = tree_2.heralded_storage()
    hs_3 = tree_3.heralded_storage()

    # Assert all stabilizers of the bit-flip code give expected results when measuring them on the trees
    z1z2 = many_tree_z_z_stab_meas([tree_1, tree_2], [hs_1, hs_2])
    assert z1z2 == 1
    z2z3 = many_tree_z_z_stab_meas([tree_2, tree_3], [hs_2, hs_3])
    assert z2z3 == 1
    z1z3 = many_tree_z_z_stab_meas([tree_1, tree_3], [hs_1, hs_3])
    assert z1z3 == 1

    # Decode all three trees
    d_tree_1 = tree_1.decode_tree(hs_1[0], hs_1[1])
    d_tree_2 = tree_2.decode_tree(hs_2[0], hs_2[1])
    d_tree_3 = tree_3.decode_tree(hs_3[0], hs_3[1])

    # Check that the decoding was successful
    assert d_tree_1 is True
    assert d_tree_2 is True
    assert d_tree_3 is True

    # Assert if the state now is the same as in the reference
    assert np.isclose(qapi.fidelity([hs_1[1], hs_2[1], hs_3[1]], ref), 1)
