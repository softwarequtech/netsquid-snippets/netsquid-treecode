import shutil

from netsquid_treecode.tree_channel_error_probability import collect_choi_states, determine_choi_state, \
    send_through_tree_channel, channel_fidelity, choi_state_fidelity_to_channel_fidelity, \
    channel_fidelity_to_depolar_prob, fidelity_to_depolarizing_channel, process_collected_choi_states, \
    load_choi_state_data, combine_choi_state_data, process_choi_state_data, collect_choi_states_from_csv
import netsquid.qubits.qubitapi as qapi
from netsquid.qubits.ketstates import b00
import netsquid as ns
import numpy as np
import pandas as pd
import os


def test_send_through_tree_channel(branching_vector=[2, 2]):
    ns.qubits.set_qstate_formalism(ns.QFormalism.DM)

    q, = qapi.create_qubits(1)
    reference_state = qapi.reduced_dm([q])
    qout = send_through_tree_channel(qubit=q, branching_vector=branching_vector, loss_probability=0,
                                     depolar_probability=0)
    assert qout is not None
    assert np.isclose(qapi.fidelity([qout], reference_state, squared=True), 1.)

    q, = qapi.create_qubits(1)
    qout = send_through_tree_channel(qubit=q, branching_vector=branching_vector, loss_probability=1,
                                     depolar_probability=0)
    assert qout is None


def test_determine_choi_state(branching_vector=[2, 2]):
    ns.qubits.set_qstate_formalism(ns.QFormalism.DM)

    qs = determine_choi_state(branching_vector=branching_vector, loss_probability=0, depolar_probability=0)
    assert np.isclose(qapi.fidelity(qs, b00, squared=True), 1.)

    qs = determine_choi_state(branching_vector=branching_vector, loss_probability=0, depolar_probability=1)
    assert np.isclose(qapi.fidelity(qs, b00, squared=True), .25)

    qs = determine_choi_state(branching_vector=branching_vector, loss_probability=1, depolar_probability=1)
    assert qs[1] is None


def test_collect_choi_states(branching_vector=[2, 2], number_of_states=10, filename="test_choi_state.csv"):
    ns.qubits.set_qstate_formalism(ns.QFormalism.STAB)
    collect_choi_states(branching_vector=branching_vector, loss_probability=.3, depolar_probability=0.,
                        number_of_states=number_of_states, filename=filename)
    df = pd.read_csv(filename)
    check_choi_state_data(branching_vector=branching_vector, number_of_states=number_of_states, df=df)
    os.remove(filename)


def check_choi_state_data(branching_vector, number_of_states, df):
    assert len(df.columns) == 4 + len(branching_vector)
    assert (df.columns[-4:] == ["loss_prob", "depolar_prob", "choi_state", "computation_time"]).all()
    assert len(df["b0"]) == number_of_states
    assert len(df["choi_state"]) == number_of_states


def test_channel_fidelity():
    ns.qubits.set_qstate_formalism(ns.QFormalism.DM)
    qs = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate(qs, b00)
    perfect_choi_state = qapi.reduced_dm(qs)  # corresponds to identity channel
    assert np.isclose(channel_fidelity(perfect_choi_state), 1)
    qapi.depolarize(qs[0], 1)
    depolarized_choi_state = qapi.reduced_dm(qs)  # corresponds to depolarizing channel (depolar prob = 1)
    assert np.isclose(channel_fidelity(depolarized_choi_state), .5)


def test_choi_state_fidelity_to_channel_fidelity():
    assert np.isclose(choi_state_fidelity_to_channel_fidelity(1.), 1.)  # identity channel
    assert np.isclose(choi_state_fidelity_to_channel_fidelity(.25), .5)  # depolar prob = 1
    # note: depolarizing creates the state I / 4 with fidelity 1 / 4, but as a single-qubit channel it always
    # creates I / 2 with fidelity 1 / 2. Therefore Choi-state fidelity .25 should map to channel fidelity .5.


def test_channel_fidelity_to_depolar_prob():
    assert channel_fidelity_to_depolar_prob(1., 0.) == (0., 0.)
    assert channel_fidelity_to_depolar_prob(.5, 0.) == (1., 0.)
    assert channel_fidelity_to_depolar_prob(.8, .1)[1] == .2


def test_fidelity_to_depolarizing_channel():

    # Check that the fidelity to depolarizing channel is always one for an actual depolarizing channel
    qs = qapi.create_qubits(2, no_state=True)
    for depolar_prob in [0., 0.4, 1.]:
        qapi.assign_qstate(qs, b00)
        qapi.depolarize(qs[1], depolar_prob)
        choi_state_depolar_channel = qapi.reduced_dm(qs)
        assert np.isclose(fidelity_to_depolarizing_channel(choi_state_depolar_channel, depolar_prob), 1.)

    # Check that the fidelity to depolarizing channel is not one for a dephasing channel
    qapi.assign_qstate(qs, b00)
    dephasing_prob = 0.2
    qapi.dephase(qs[1], dephasing_prob)
    choi_state_dephasing_channel = qapi.reduced_dm(qs)
    assert not np.isclose(fidelity_to_depolarizing_channel(choi_state_dephasing_channel, dephasing_prob), 1.)
    # also try other values of depolar prob to be sure
    assert not np.isclose(fidelity_to_depolarizing_channel(choi_state_dephasing_channel, 2 * dephasing_prob), 1.)
    assert not np.isclose(fidelity_to_depolarizing_channel(choi_state_dephasing_channel, .5 * dephasing_prob), 1.)


def test_process_collected_choi_states(branching_vector=[2, 2]):
    ns.qubits.set_qstate_formalism(ns.QFormalism.DM)

    success_prob, success_prob_error, depolar_prob, depolar_prob_error, fid_to_depolar_channel \
        = collect_and_process_choi_states(branching_vector=branching_vector,
                                          loss_probability=0., depolar_probability=0.)
    assert np.isclose(success_prob, 1.)
    assert np.isclose(success_prob_error, 0.)
    assert np.isclose(depolar_prob, 0.)
    assert np.isclose(depolar_prob_error, 0.)
    assert np.isclose(fid_to_depolar_channel, 1.)

    success_prob, success_prob_error, depolar_prob, depolar_prob_error, fid_to_depolar_channel \
        = collect_and_process_choi_states(branching_vector=branching_vector,
                                          loss_probability=1., depolar_probability=0.)
    assert np.isclose(success_prob, 0.)
    assert np.isclose(success_prob_error, 0.)
    assert depolar_prob is None
    assert depolar_prob_error is None
    assert fid_to_depolar_channel is None

    success_prob, success_prob_error, depolar_prob, depolar_prob_error, fid_to_depolar_channel \
        = collect_and_process_choi_states(branching_vector=branching_vector,
                                          loss_probability=0., depolar_probability=1.)
    assert np.isclose(success_prob, 1.)
    assert np.isclose(success_prob_error, 0.)
    assert np.isclose(depolar_prob, 1.)
    assert np.isclose(depolar_prob_error, 0.)
    assert np.isclose(fid_to_depolar_channel, 1.)

    success_prob, success_prob_error, depolar_prob, depolar_prob_error, fid_to_depolar_channel \
        = collect_and_process_choi_states(branching_vector=branching_vector,
                                          loss_probability=0., depolar_probability=.4)
    assert not np.isclose(depolar_prob, 1.)
    assert not np.isclose(depolar_prob, 0.)


def collect_and_process_choi_states(branching_vector, loss_probability, depolar_probability):
    choi_states = []
    for _ in range(10):
        qs = determine_choi_state(branching_vector=branching_vector, loss_probability=loss_probability,
                                  depolar_probability=depolar_probability)
        if qs[1] is None:
            choi_states.append(None)
            continue
        choi_states.append(qapi.reduced_dm(qs))
    return process_collected_choi_states(choi_states)


def test_load_choi_state_data(branching_vector=[2, 2], number_of_states=10):
    filename = "test_choi_state_temporary_file.csv"
    for formalism in [ns.QFormalism.STAB, ns.QFormalism.DM, ns.QFormalism.KET, ns.QFormalism.GSLC]:
        ns.qubits.set_qstate_formalism(formalism)
        collect_choi_states(branching_vector=branching_vector, loss_probability=.1, depolar_probability=.1,
                            number_of_states=number_of_states, filename=filename)
        df = load_choi_state_data(filename)
        check_choi_state_data(branching_vector=branching_vector, number_of_states=number_of_states, df=df)
    os.remove(filename)


def create_test_data(branching_vector, number_of_states, path):
    if os.path.isdir(path):
        remove_test_data(path)  # make sure state is clean
    os.mkdir(path)
    collect_choi_states(branching_vector=branching_vector, loss_probability=.1, depolar_probability=.1,
                        number_of_states=number_of_states, filename=path + "/test0.test.csv")
    collect_choi_states(branching_vector=branching_vector, loss_probability=.2, depolar_probability=.1,
                        number_of_states=number_of_states, filename=path + "/test1.test.csv")
    collect_choi_states(branching_vector=branching_vector, loss_probability=.1, depolar_probability=.2,
                        number_of_states=number_of_states, filename=path + "/test2.test.csv")
    collect_choi_states(branching_vector=branching_vector, loss_probability=.2, depolar_probability=.2,
                        number_of_states=number_of_states, filename=path + "/test3.csv")


def remove_test_data(path):
    for i in range(3):
        os.remove(path + f"/test{i}.test.csv")
    os.remove(path + "/test3.csv")
    os.rmdir(path)


def test_combine_choi_state_data(branching_vector=[2, 2], number_of_states=3, path="./test_data",
                                 filename="combine_test.csv"):
    ns.qubits.set_qstate_formalism(ns.QFormalism.STAB)
    create_test_data(branching_vector=branching_vector, number_of_states=number_of_states, path=path)
    combine_choi_state_data(path=path, suffix=".test.csv", output_filename=filename)
    combined_data = load_choi_state_data(filename)
    check_choi_state_data(branching_vector=branching_vector, number_of_states=number_of_states * 3, df=combined_data)
    remove_test_data(path)
    os.remove(filename)


def test_process_choi_state_data(branching_vector=[2, 2], number_of_states=10, path="./test_data",
                                 combined_filename="combined_test_data.csv",
                                 processed_filename="processed_test_data.csv"):
    ns.qubits.set_qstate_formalism(ns.QFormalism.STAB)
    create_test_data(branching_vector=branching_vector, number_of_states=number_of_states, path=path)
    combine_choi_state_data(path=path, suffix=".test.csv", output_filename=combined_filename)
    process_choi_state_data(data_filename=combined_filename, output_filename=processed_filename)
    processed_data = pd.read_csv(processed_filename)
    assert len(processed_data["depolar_prob"]) == 3
    assert len(processed_data["depolar_prob"].unique()) == 2
    assert len(processed_data["loss_prob"]) == 3
    assert len(processed_data["loss_prob"].unique()) == 2
    assert (processed_data.columns[-6:] == ["success_prob", "success_prob_error", "reencoding_error_prob",
                                            "reencoding_error_prob_error", "fidelity_to_depolar_channel",
                                            "computation_time"]).all()
    remove_test_data(path)
    os.remove(combined_filename)
    os.remove(processed_filename)


def test_collect_choi_states_from_csv(filename="test_csv_file.csv", path="test_csv_path", number_of_states=100):
    if os.path.isdir(path):
        shutil.rmtree(path)
    specification = {"b0": [2, 2, 2, 2], "b1": [2, 2, 2, 2], "L_0[km]": [10, 10, 10, 20],
                     "physical_error_prob": [0.1, 0.1, 0.2, 0.1]}
    df = pd.DataFrame(specification)
    df.to_csv(filename, index=False)
    collect_choi_states_from_csv(csv_specification_filename=filename, number_of_states=number_of_states, path=path)
    combined_data = pd.read_csv(path + "/combined_data.csv")
    assert len(combined_data["b0"]) == 4 * number_of_states
    processed_data = pd.read_csv(path + "/processed_data.csv")
    assert len(processed_data["b0"]) == 3  # three unique values of varied parameters
    os.remove(filename)
    shutil.rmtree(path)
