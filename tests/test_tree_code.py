from netsquid_treecode.tree_code import flatten_list
from netsquid_treecode.tree_code import Tree
from netsquid_treecode.models import PureLossModel
import numpy as np
import copy
import netsquid.qubits.operators as ops
import netsquid.qubits.qubitapi as qapi
import netsquid as ns
import netsquid.qubits.stabtools as qstab


def test_tree_class_depth_1(branching_vector=[4], angle=np.pi / 4, axis=(0, 0, 1)):
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)
    data_q, = qapi.create_qubits(1, 'Data')  # create the data qubit
    init = ops.create_rotation_op(angle, axis)  # define rotation operation
    qapi.operate(data_q, init)  # initialize the data qubit
    # the data qubit can be expressed as a|0>+b|1>, here are a and b
    a = complex(np.cos(angle / 2), np.sin(angle / 2) * axis[2])
    b = complex(-np.sin(angle / 2) * axis[1], -np.sin(angle / 2) * axis[0])
    corrected_state = Tree(data_q, branching_vector)
    # Put all the first-level qubits in a list
    leaves = [corrected_state.get_qubit([i]) for i in range(branching_vector[0])]
    for leaf in leaves:
        qapi.operate(leaf, ops.H)  # this way the states will be a|00...0> + b|11...1>
    # we create a ket vector of the expected state for the leave qubits
    dim = 2 ** (branching_vector[0])
    expected_state = np.zeros((dim, 1), dtype=complex)
    expected_state[0] = a
    expected_state[dim - 1] = b
    assert np.isclose(qapi.fidelity(leaves, expected_state), 1)


def test_tree_class_depth_2(branching_vector=[3, 2], angle=np.pi / 4, axis=(0, 0, 1)):
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)
    data_q, = qapi.create_qubits(1, 'Data')  # create the data qubit
    init = ops.create_rotation_op(angle, axis)  # define rotation operation
    qapi.operate(data_q, init)  # initialize the data qubit
    # the data qubit can be expressed as a|0>+b|1>, here are a and b
    a = complex(np.cos(angle / 2), np.sin(angle / 2) * axis[2])
    b = complex(-np.sin(angle / 2) * axis[1], -np.sin(angle / 2) * axis[0])
    # Encode and correct the data qubit in a tree state
    corrected_state = Tree(data_q, branching_vector)
    first_leaves = [corrected_state.get_qubit([i]) for i in range(branching_vector[0])]
    second_leaves = [corrected_state.get_qubit([i, j]) for i in range(branching_vector[0])
                     for j in range(branching_vector[1])]
    for leaf in second_leaves:
        # Turn each sub-tree in the first part of the state of the tree into a GHZ state |000...0> + |111...1>
        # and each sub-tree in the second part of the state of the tree into a state like |000...0> - |111...1>
        qapi.operate(leaf, ops.H)
    # We create a list to organize the qubits as in the KET state in the paper. Meaning, each of the subtrees
    # with the first level qubit first.
    tree_ket_state = []
    i = 0  # Counter for the first level qubits
    j = 0  # Counter for the second level qubits in each sub-tree
    k = 0  # Counter for the second level qubits in total
    while i < len(first_leaves):
        tree_ket_state.append(first_leaves[i])  # first we append the first level qubit of a sub-tree
        while j < branching_vector[1]:
            tree_ket_state.append(second_leaves[k])  # then we append the second level qubits of that sub-tree
            j += 1
            k += 1
        j = 0  # reset the counter as this one moves for each of the second-level qubits in a sub-tree
        i += 1
    dim = 2 ** (branching_vector[1] + 1)  # dimension for the ket state of a sub-tree
    exp_ket_plus = np.zeros((dim, 1))  # create the |000...0> + |111...1>
    exp_ket_plus[0] = 1
    exp_ket_plus[dim - 1] = 1
    exp_ket_plus_tot = exp_ket_plus
    exp_ket_min = np.zeros((dim, 1))  # create the |000...0> - |111...1>
    exp_ket_min[0] = 1
    exp_ket_min[dim - 1] = -1
    exp_ket_min_tot = exp_ket_min
    for i in range(len(first_leaves) - 1):
        # multiply the |000...0> + |111...1> state for as many first-level qubits
        exp_ket_plus_tot = np.kron(exp_ket_plus_tot, exp_ket_plus)
        # multiply the |000...0> - |111...1> state for as many first-level qubits
        exp_ket_min_tot = np.kron(exp_ket_min_tot, exp_ket_min)
    # Next we define the factors normalization factors a1 and b1
    a1 = a / (np.sqrt(2) ** (branching_vector[0]))
    b1 = b / (np.sqrt(2) ** (branching_vector[0]))
    expected_ket = a1 * exp_ket_plus_tot + b1 * exp_ket_min_tot
    assert np.isclose(qapi.fidelity(tree_ket_state, expected_ket), 1)


def test_decode_depth_2(branching_vector=[2, 2], angle=np.pi / 4, axis=(0, 0, 1)):
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)
    data_q, = qapi.create_qubits(1, 'Data')  # Create the data qubit
    init = ops.create_rotation_op(angle, axis)  # Define rotation operation
    qapi.operate(data_q, init)  # initialize the data qubit
    ref = copy.deepcopy(data_q.qstate.ket)  # We copy the state of the data qubit before encoding as the reference state
    tree = Tree(data_q, branching_vector)  # Encode data in a tree and we also perform the corrections
    hs = tree.heralded_storage()
    tree.decode_tree(hs[0], hs[1])  # Decode the tree
    assert np.isclose(qapi.fidelity(hs[1], ref), 1)


def test_decode_depth_3(branching_vector=[2, 2, 2], angle=np.pi / 4, axis=(0, 0, 1)):
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)
    data_q, = qapi.create_qubits(1, 'Data')  # Create the data qubit
    init = ops.create_rotation_op(angle, axis)  # Define rotation operation
    qapi.operate(data_q, init)  # initialize the data qubit
    ref = copy.deepcopy(data_q.qstate.ket)  # We copy the state of the data qubit before encoding as the reference state
    tree = Tree(data_q, branching_vector)  # Encode data in a tree and we also perform the corrections
    hs = tree.heralded_storage()
    tree.decode_tree(hs[0], hs[1])  # Decode the tree
    assert np.isclose(qapi.fidelity(hs[1], ref), 1)


def test_heralded_storage(branching_vector=[3, 2], angle=np.pi / 4, axis=(0, 0, 1)):
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.STAB)
    data_q, = qapi.create_qubits(1, 'Data')  # Create the data qubit
    init = ops.create_rotation_op(angle, axis)  # Define rotation operation
    qapi.operate(data_q, init)  # initialize the data qubit
    tree = Tree(data_q, branching_vector)  # Encode data in a tree and we also perform the corrections
    stab = (tree.get_qubit([0])).qstate
    ref_stab_matrix = copy.deepcopy(stab.stabilizer.check_matrix)
    ref_stab_phases = copy.deepcopy(stab.stabilizer.phases)
    ref_stab = qstab.Stabilizer(ref_stab_matrix, ref_stab_phases)
    hs = tree.heralded_storage()
    qubits = []
    for i in range(branching_vector[0]):
        qubits.append(tree.get_qubit([i]))
        for j in range(branching_vector[1]):
            qubits.append(tree.get_qubit([i, j]))
    qubits[0] = hs[1]
    stab = hs[1].qstate
    assert np.isclose(ns.qubits.stabtools.Stabilizer.fidelity(stab.stabilizer, ref_stab), 1)


def test_z_on_heralded_storage_subtree(branching_vector=[3, 2], angle=np.pi / 4, axis=(0, 0, 1)):
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.STAB)
    data_q, = qapi.create_qubits(1, 'Data')  # Create the data qubit
    init = ops.create_rotation_op(angle, axis)  # Define rotation operation
    qapi.operate(data_q, init)  # initialize the data qubit
    tree = Tree(data_q, branching_vector)  # Encode data in a tree and we also perform the corrections
    stab = (tree.get_qubit([0])).qstate
    ref_stab_matrix = copy.deepcopy(stab.stabilizer.check_matrix)
    ref_stab_phases = copy.deepcopy(stab.stabilizer.phases)
    ref_stab = qstab.Stabilizer(ref_stab_matrix, ref_stab_phases)
    hs = tree.heralded_storage()
    qubits = []
    tree.z_on_tree_using_heralded_storage_subtree(hs[0], hs[1])
    tree.z_on_tree_using_heralded_storage_subtree(hs[0], hs[1])
    for i in range(branching_vector[0]):
        qubits.append(tree.get_qubit([i]))
        for j in range(branching_vector[1]):
            qubits.append(tree.get_qubit([i, j]))
    qubits[0] = hs[1]
    stab = hs[1].qstate
    assert np.isclose(ns.qubits.stabtools.Stabilizer.fidelity(stab.stabilizer, ref_stab), 1)


def test_z_operation(branching_vector=[3, 2], angle=np.pi / 4, axis=(0, 0, 1)):
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.STAB)
    data_q, = qapi.create_qubits(1, 'Data')  # Create the data qubit
    init = ops.create_rotation_op(angle, axis)  # Define rotation operation
    qapi.operate(data_q, init)  # initialize the data qubit
    tree = Tree(data_q, branching_vector)  # Encode data in a tree and we also perform the corrections
    stab = (tree.get_qubit([0])).qstate
    ref_stab_matrix = copy.deepcopy(stab.stabilizer.check_matrix)
    ref_stab_phases = copy.deepcopy(stab.stabilizer.phases)
    ref_stab = qstab.Stabilizer(ref_stab_matrix, ref_stab_phases)
    tree._z_operation_qubit([0, 1])
    tree._z_operation_qubit([0, 1])
    assert np.isclose(ns.qubits.stabtools.Stabilizer.fidelity(stab.stabilizer, ref_stab), 1)


def test_z_operation_losses_depth_2(branching_vector=[2, 2], angle=np.pi / 4, axis=(0, 0, 1)):
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.STAB)
    data_q, = qapi.create_qubits(1, 'Data')  # Create the data qubit
    init = ops.create_rotation_op(angle, axis)  # Define rotation operation
    qapi.operate(data_q, init)  # initialize the data qubit
    tree = Tree(data_q, branching_vector)  # Encode data in a tree and we also perform the corrections
    tree._z_operation_qubit([1])
    tree.set_qubit_used([1])
    stab = (tree.get_qubit([0])).qstate
    ref_stab_matrix = copy.deepcopy(stab.stabilizer.check_matrix)
    ref_stab_phases = copy.deepcopy(stab.stabilizer.phases)
    ref_stab = qstab.Stabilizer(ref_stab_matrix, ref_stab_phases)
    tree._z_operation_qubit([1])
    tree._z_operation_qubit([1])
    assert np.isclose(ns.qubits.stabtools.Stabilizer.fidelity(stab.stabilizer, ref_stab), 1)


def test_z_operation_losses_depth_3(branching_vector=[2, 2, 2], angle=np.pi / 4, axis=(0, 0, 1)):
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.STAB)
    data_q, = qapi.create_qubits(1, 'Data')  # Create the data qubit
    init = ops.create_rotation_op(angle, axis)  # Define rotation operation
    qapi.operate(data_q, init)  # initialize the data qubit
    tree = Tree(data_q, branching_vector)  # Encode data in a tree and we also perform the corrections
    tree._z_operation_qubit([1])
    tree.set_qubit_used([1])
    stab = (tree.get_qubit([0])).qstate
    ref_stab_matrix = copy.deepcopy(stab.stabilizer.check_matrix)
    ref_stab_phases = copy.deepcopy(stab.stabilizer.phases)
    ref_stab = qstab.Stabilizer(ref_stab_matrix, ref_stab_phases)
    tree._z_operation_qubit([1])
    tree._z_operation_qubit([1])
    assert np.isclose(ns.qubits.stabtools.Stabilizer.fidelity(stab.stabilizer, ref_stab), 1)


def test_get_qubit():
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)
    qubit, = qapi.create_qubits(1)
    tree = Tree(qubit, [2, 2])
    for i, j in zip(range(2), range(2)):
        assert isinstance(tree.get_qubit([i, j]), ns.qubits.qubit.Qubit)


def test_set_qubit_used():
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)
    qubit, = qapi.create_qubits(1)
    tree = Tree(qubit, [3, 2])
    assert tree.get_qubit([0]) is not None
    tree.set_qubit_used([0])
    assert tree.get_qubit([0]) is None


def test_flatten_list():
    nested_list = [0, 1, [2], [3, [4, 5]], [6, [7, 8, [9, [10, [11]]]]]]
    flat_list = flatten_list(nested_list)
    for i in range(12):
        assert i in flat_list


def check_decoding_after_losses(branching_vector, lost_qubit_position_vectors, expect_success, lose_all_qubits=False):
    """Check if decoding works after some qubits in the tree have been lost.

    Parameters
    ----------
    branching_vector : list of int
        Branching vector characterizing the tree code.
    lost_qubit_position_vectors : list of lists
        Position vectors of qubits that are lost.
    expect_success : bool
        True if decoding should still work after losing these qubits.
    lose_all_qubits : bool, optional
        Set to true if all qubits in the tree should be lost. Overrides `lost_qubit_position_vectors`.

    """
    qinit, = qapi.create_qubits(1)
    qapi.operate(qinit, ops.H)
    ref = copy.deepcopy(qinit.qstate.ket)
    tree = Tree(qinit, branching_vector)
    if lose_all_qubits:
        lost_qubits = tree.qubits
    else:
        lost_qubits = [tree.get_qubit(lost_qubit_position_vector)
                       for lost_qubit_position_vector in lost_qubit_position_vectors]
    PureLossModel(1)(lost_qubits)
    decoded_qubit = tree.store_and_decode_tree()
    if expect_success:
        assert decoded_qubit is not None
        assert np.isclose(qapi.fidelity(decoded_qubit, ref), 1)
    else:
        assert decoded_qubit is None


def test_decode_losses():
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)
    check_decoding_after_losses(branching_vector=[2, 2], expect_success=True,
                                lost_qubit_position_vectors=[[1], [1, 0]])
    check_decoding_after_losses(branching_vector=[2, 2], expect_success=True,
                                lost_qubit_position_vectors=[[1], [1, 1]])
    check_decoding_after_losses(branching_vector=[2, 2], expect_success=True,
                                lost_qubit_position_vectors=[[1, 0], [1, 1]])
    check_decoding_after_losses(branching_vector=[2, 2], expect_success=False,
                                lost_qubit_position_vectors=[[1], [1, 0], [1, 1]])
    check_decoding_after_losses(branching_vector=[3, 3], expect_success=False, lost_qubit_position_vectors=[],
                                lose_all_qubits=True)
    check_decoding_after_losses(branching_vector=[2, 2, 2], expect_success=False, lost_qubit_position_vectors=[],
                                lose_all_qubits=True)
    check_decoding_after_losses(branching_vector=[2, 2, 2], expect_success=True, lost_qubit_position_vectors=[]),
